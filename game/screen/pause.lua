-- Entité représentant l'écran de Pause
-- Utilise une forme d'héritage simple: screenStart qui inclus screen
-- ****************************************
-- TYPE D'ECRAN
SCREEN_PAUSE = "screenPause"

--- Création de l'écran
-- @param pName nom de l'écran. Si absent, la valeur SCREEN_UNDEFINED sera utilisée
-- @param pBgImage (OPTIONNEL) image de fond.
-- @param pMusic (OPTIONNEL) musique
-- @return un pseudo objet screenPause
function createScreenPause (pName, pBgImage, pMusic)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreenPause", pName, pBgImage, pMusic)

  local lScreen = createScreen(SCREEN_PAUSE, pBgImage, pMusic)

  -- Override des fonctions de l'écran
  -- ******************************
  --[[ à décommenter pour des comportements spécifiques
  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  lScreen.screenInitialize= lScreen.initialize -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.initialize (pBgImage, pMusic)
    debugFunctionEnter("screenPause.initialize ", pBgImage, pMusic)
    lScreen.screenInitialize(pBgImage, pMusic)

    lScreen.isInitialized = true
  end -- screen.initialize

  --- Actualise l'écran
  -- @param pDt delta time
  lScreen.screenUpdate= lScreen.update -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screenPause.update") -- ATTENTION cet appel peut remplir le log
    lScreen.screenUpdate(pDt)
  end -- screen.update

  --- affiche l'écran
  lScreen.screenShow = lScreen.show -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.show (pHasWon)
    debugFunctionEnter("screenPause.show")
    lScreen.screenShow()
  end -- screen.show
  ]]

  --- Dessine l'écran
  lScreen.screenDraw = lScreen.draw -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screenPause.draw") -- ATTENTION cet appel peut remplir le log
    lScreen.screenDraw()
    love.graphics.push()
    love.graphics.scale(DISPLAY_SCALE_X, DISPLAY_SCALE_X)

    local w, h, font, i
    local color, content
    font = HUD.get_fontMenuContent()
    w = font:getHeight(" ")
    h = font:getHeight(" ")
    local offsetX = 0
    local offsetY = 0

    font = HUD.get_fontMenuTitle()
    color = {255, 160, 0, 255} -- couleur orange sans transparence
    content = "JEU en PAUSE"
    HUD.displayTextScale(content, font, POS_CENTER, 0, 0, color)

    font = HUD.get_fontMenuContent()
    content = 'Appuyer sur "'..settings.appKeys.pauseGame..'" pour reprendre'
    HUD.displayTextScale(content, font, POS_CENTER, 0, h * 3)

    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.pop()
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param TODO: description
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screenPause.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    if (pKey == settings.appKeys.pauseGame) then screenPlay.show(false, false) end
  end -- screen.keypressed

  --[[ à décommenter pour des comportements spécifiques
  --- Gestion du "mousepressed" sur l'écran
  -- @param TODO: description
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screenPause.mousepressed")
    -- rien à faire pour le moment
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param TODO: description
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screenPause.mousemoved ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end -- screen.mousemoved
  ]]

  return lScreen
end -- createScreenPause