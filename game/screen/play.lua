-- Entité représentant l'écran de jeu
-- Utilise une forme d'héritage simple: screenPlay qui inclus screen
-- ****************************************
-- TYPE D'ECRAN
SCREEN_PLAY  = "screenPlay"

--- Création de l'écran
-- @param pName nom de l'écran. Si absent, la valeur SCREEN_UNDEFINED sera utilisée
-- @param pBgImage (OPTIONNEL) image de fond.
-- @param pMusic (OPTIONNEL) musique
-- @return un pseudo objet screenPlay
function createScreenPlay (pName, pBgImage, pMusic)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreenPlay", pName, pBgImage, pMusic)

  local lScreen = createScreen(SCREEN_PLAY, pBgImage, nil) -- pas de musique de fond sur cet écran car gérée par le musicManager pour chaque niveau

  -- doit on restaurer les infos du joueur ?
  lScreen.mustRestorePlayer = false

  -- Override des fonctions de l'écran
  -- ******************************

  --- Initialise l'écran
  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMustRestorePlayer (OPTIONNEL) True pour restaurer les infos du joueur pendant l'initialisation de l'écran.
  lScreen.screenInitialize= lScreen.initialize -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.initialize (pBgImage, pMusic, pMustRestorePlayer)
    debugFunctionEnter("screenPlay.initialize ", pBgImage, pMusic, pMustRestorePlayer)
    lScreen.screenInitialize(pBgImage, pMusic)

    debugInfos = ""

    if (not appState.hasStarted) then
      -- cas ou on n'est pas passé par le menu de départ (screenStart)
      appState.initialize(20) -- niveau 20 pour gagner
      appState.hasStarted = true -- important
    end

    -- Entité représentant la carte
    -- NOTE: les sprites seront réinitialisés dans le createGameMap
    map = createGameMap(pLevel)

    -- la carte ne peut pas être nil
    assertEqualQuit(map, nil, "startGame:La map n'a pas été initialisée correctement", true)

    -- initialisation propre à chaque prototype
    -- ******************
    gamePrototype.startGame()

    -- Entité représentant le joueur
    player = createPlayer()

    -- initialisation commune à tous les prototypes
    -- ******************
    player.updateInitialValues()

    assertEqualQuit(HUD, nil, "lScreen.initialize:HUD", true)
    assertEqualQuit(viewport, nil, "lScreen.initialize:viewport", true)
    assertEqualQuit(settings, nil, "lScreen.initialize:settings", true)
    assertEqualQuit(appState, nil, "lScreen.initialize:appState", true)
    assertEqualQuit(player, nil, "lScreen.initialize:player", true)

    if (pMustRestorePlayer) then
      player = appState.restorePlayer(appState.currentPlayerId)
      pLevel = player.level
    else
      pLevel = 1
    end

    map.createLevelContent(pLevel)

    if (map.mapIsOK) then
      musicManager.play(pLevel)

      HUD.autoHideMessage.color = {255, 128, 0, 255}
      HUD.autoHideMessage.text = "Niveau "..pLevel
      HUD.autoHideMessage.timer = 0

      -- place le joueur dans la carte
      if (map.type == MAP_TYPE_NOT_TILED) then
        player.spawn()
      else
        -- player.spawnToMap(map.playerStart().col, map.playerStart().line)
        player.spawnToMap()
      end
    else
      logError("Le niveau "..pLevel.." de la carte n'a pas été chargé correctement")
    end

    lScreen.isInitialized = true
  end -- screen.initialize

  --- Actualise l'écran
  -- @param pDt delta time
  lScreen.screenUpdate= lScreen.update -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screenPlay.update") -- ATTENTION cet appel peut remplir le log
    lScreen.screenUpdate(pDt)

    local index, sprite
    -- CAMERA : DÉPLACEMENTS EN FONCTION DE SA VITESSE
    -- ******************************
    camera.update(pDt)

    -- MAP : mise a jour (spawn des éléments et pnj)
    -- ******************************
    map.update(pDt)

    -- BOUCLE DE TRAITEMENT ET DE SUPPRESSION DES SPRITES
    -- ******************************
    for index = #entities.sprites, 1, -1  do -- le parcours décroissant permet de gérer un table.remove dans une liste en cours de parcours
      sprite = entities.sprites[index]

      local spriteIsPlayer = (sprite.type:find(SPRITE_TYPE_CHARACTER) ~= nil)
      local spriteIsNpc = (sprite.type:find(SPRITE_TYPE_NPC) ~= nil)
      local spriteIsBoss = (sprite.type:find(SPRITE_TYPE_NPC_BOSS) ~= nil)

      sprite.update(pDt) -- note: contient l'appel aux fonctions update() et collide() de chaque type de sprite

      -- le sprite est-il en cours de destruction ?
      if (sprite.status == SPRITE_STATUS_DESTROYING and sprite.destroyDelay ~= -1) then
          -- le délai de destruction est il écoulé ?
        if (sprite.destroyDelay <= 0.2 ) then -- ici 0 ne fonctionne ne pas WTF ?
          -- oui, on le marque à détruire
          sprite.clean()
        else
          sprite.destroyDelay  = sprite.destroyDelay - pDt
        end
      end

      -- le sprite doit-il être supprimé ?
      if (sprite.status == SPRITE_STATUS_DESTROYED) then
        -- oui, suppression du sprite de la liste
        table.remove(entities.sprites, index)
        if (spriteIsPlayer) then
          table.remove(entities.characters, sprite.id)
          -- si c'est le joueur, la partie est perdue
          screenLose.show()
          return
        elseif (spriteIsBoss and sprite.mustChangeLevelIfDestroyed) then -- important, cette ligne doit être AVANT la ligne elseif (spriteIsNpc) then
          -- si c'est un boss, on passe au niveau suivant
          nextLevel()
          return
        elseif (spriteIsNpc) then
          table.remove(entities.NPCs, sprite.id)
          if (map.enemiesDestroyed ~= nil) then map.enemiesDestroyed = map.enemiesDestroyed + 1 end
          -- debugMessage ("table.remove(entities.NPCs,".. sprite.id)
        else
          sprite = nil
        end
      else
        sprite.listIndex = index -- important pour maintenir l'index dans la table
        -- debugMessage("---- - KEEP", index, #entities.sprites)
      end
    end -- for index = #entities.sprites

    -- BOUCLE DE SUPPRESSION DES ANIMATIONS
    -- ******************************
    local animation
    for index = #entities.animations, 1, -1  do -- le parcours décroissant permet de gérer un table.remove dans une liste en cours de parcours
      animation = entities.animations[index]
      -- l'animation est-elle en cours de destruction ?
      if (animation.status == ANIM_STATUS_DESTROYING) then
        -- oui, on la marque à détruire
        animation.clean()
      end

      -- l'animation doit-il être supprimée ?
      if (animation.status == ANIM_STATUS_DESTROYED) then
        -- oui, suppression de l'animation de la liste
        table.remove(entities.animations, index)
        animation = nil
      else
        animation.listIndex = index -- important pour maintenir l'index dans la table
      end
    end -- for index = #entities.animations

    -- INFOS DE DEBUGGAGE
    -- ******************************
    debugInfos = "joueur: X= "..math.floor(player.x).." Y= "..math.floor(player.y).." W= "..math.floor(player.width).." H= "..math.floor(player.height)
    debugInfos = debugInfos.."\nvX = "..math.floor(player.vX).."\nvY= "..math.floor(player.vY).."\nSpeed= "..math.floor(player.speed)
    debugInfos = debugInfos.."\nAnim = "..(player.currentAnimation.name or "NONE").."\nframe= "..(player.currentAnimation.frame or 0)
    debugInfos = debugInfos.."\nSprites:"..#entities.sprites
  end -- updatePlayScreen

  --- affiche l'écran
  -- @param pMustReset (OPTIONNEL) true pour réinitialiser l'écran. Si absent la valeur TRUE sera utilisée.
  -- @param pMustRestorePlayer (OPTIONNEL) True pour restaurer les infos du joueur pendant l'initialisation de l'écran. Si absent la valeur FALSE sera utilisée.
  lScreen.screenShow = lScreen.show -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.show (pMustReset, pMustRestorePlayer)
    debugFunctionEnter("screenStart.show")
    if (pMustReset == nil) then pMustReset = true end
    if (pMustRestorePlayer == nil) then pMustRestorePlayer = false end

    if (pMustReset) then
      lScreen.isInitialized = true -- permet d'éviter un initialise déclenché par le lScreen.screenShow()
      lScreen.screenShow()
      lScreen.initialize(nil, nil, pMustRestorePlayer)
    else
      lScreen.screenShow()
    end
  end

  --- Dessine l'écran
  lScreen.screenDraw = lScreen.draw -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screenPlay.draw") -- ATTENTION cet appel peut remplir le log
    lScreen.screenDraw()
    love.graphics.push()
    love.graphics.scale(DISPLAY_SCALE_X, DISPLAY_SCALE_X)

    -- AFFICHAGE de la MAP
    -- ******************
    map.draw()

   -- SPRITES: affichage de tous les sprites sans distinctions
    -- ******************************
    if (entities.sprites ~= nil) then
      local indexSprite, sprite
      for indexSprite = #entities.sprites, 1, -1 do
        sprite = entities.sprites[indexSprite]

        sprite.draw()
      end
    end

    local w, h, font, i
    local color, content
    local w = HUD.get_fontGUI():getWidth("_")  * DISPLAY_SCALE_X
    local h = HUD.get_fontGUI():getHeight("|")  * DISPLAY_SCALE_Y
    local drawX = viewport.getWidth()
    local drawY = viewport.getHeight()

    -- affiche le nombre de vie du joueur
    -- s'il est négatif, on affiche rien
    if (player.life >= 0) then
      local imgPath = player.lifeImage
      local livesResource = resourceManager.get(imgPath) -- type de ressource déterminé automatiquement par l'extension de fichier
      local livesImg = livesResource.source
      local livesImgWidth = livesResource.width
      local livesImgHeight = livesResource.height

      --if (imgPath ~= "" and livesImg ~= nil) then
      if (livesResource.type ~= RESOURCE_TYPE_UNDEFINED and livesResource.type ~= RESOURCE_TYPE_NOTFOUND) then

        -- si le joueur a une image, on affiche l'image pour les vies restantes
        love.graphics.setColor(255, 255, 255, 255) -- pour que les images soit visibles

        local i
        drawX = viewport.Xmax - (livesImgWidth + 5) * player.initialValues.life - 5
        drawY = 0
        for i = 0, player.life- 1 do
          HUD.drawImageScale(livesImg, drawX + (livesImgWidth + 5) * i, drawY, 0, 1, 1, 0, 0, 0, 5)
        end
      else
        -- sinon affiche un texte
        HUD.displayTextScale("Vies "..player.life, HUD.get_fontGUI(), POS_TOP_RIGHT, -10, 5, {viewport.textColor[1], viewport.textColor[2], viewport.textColor[3], 255})
      end
    end

    -- affiche le score du joueur
    -- s'il est négatif, on affiche rien
    if (player.score >= 0) then
      drawY = 5
      HUD.displayTextScale("Score "..player.score, HUD.get_fontGUI(), POS_TOP_LEFT, 10, drawY, {255, 160, 0, 255}) -- couleur orange sans transparence
    end

    -- affiche le niveau du joueur
    -- s'il est négatif, on affiche rien
    if (player.level >= 0) then
      local rectWidth = 12
      local rectHeight = 25
      content = "Niveau "
      HUD.displayTextScale(content, HUD.get_fontGUI(), POS_BOTTOM_LEFT, 5, -1, {255, 160, 0, 255}) -- couleur orange sans transparence

      drawX = HUD.get_fontGUI():getWidth(content) * DISPLAY_SCALE_X
      drawY = viewport.getHeight()
      for i = 0, appState.levelToWin - 1 do
        if (i < player.level) then
          love.graphics.setColor(255, 160, 0, 255) -- couleur orange sans transparence
        else
          love.graphics.setColor(viewport.textColor[1], viewport.textColor[2], viewport.textColor[3], 128)
        end
        HUD.rectangleScale("fill", drawX + (i * rectWidth), drawY, rectWidth - 1, rectHeight, 0 , -rectHeight - 5)
      end
    end

    -- affiche le nom du niveau de la map
    --[[
    content = "Level "..map.index
    if (map.names ~= nil and map.names[map.index] ~= nil) then content = content ..": "..map.names[map.index] end
    HUD.displayTextScale(content, HUD.get_fontGUI(), POS_TOP_CENTER, 0, 5, {255, 255, 255, 255}) -- couleur blanc sans transparence
    ]]

    -- affiche le nombre d'ennemis
    content = "Ennemis "..map.enemiesDestroyed.."/"..map.enemiesToDestroy
    HUD.displayTextScale(content, HUD.get_fontGUI(), POS_TOP_CENTER, 0, 5, {255, 255, 255, 255}) -- couleur blanc sans transparence

    -- affichage des infos de debug (en bas de l'écran)
    if (DEBUG_MODE >= 2 and debugInfos ~= "") then
      drawY = HUD.get_fontDebug():getHeight(debugInfos)
      local _, count = string.gsub(debugInfos, "\n", "")

      HUD.displayTextScale(debugInfos, HUD.get_fontDebug(), POS_BOTTOM_RIGHT, -5, -drawY * count - 5, {0, 200, 0, 128}, 500, nil) -- vert foncé 50%
    end
    -- affichage de la dernière collision
    if (DEBUG_MODE > 0 and DEBUG_SHOW_COLLISIONS and debugLastcollision.timer > 0 ) then
      love.graphics.setColor(0, 0, 255, 255)
      HUD.rectangleScale("line", debugLastcollision.s1[1], debugLastcollision.s1[2], debugLastcollision.s1[3], debugLastcollision.s1[4])
      love.graphics.setColor(0, 255, 0, 255)
      HUD.rectangleScale("line", debugLastcollision.s2[1], debugLastcollision.s2[2], debugLastcollision.s2[3], debugLastcollision.s2[4])
      debugLastcollision.timer = debugLastcollision.timer - 0.05
    end

    love.graphics.setColor(255, 255, 255, 255) -- RAZ des couleur
    love.graphics.pop()
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param TODO: description
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screenPlay.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    if (DEBUG_MODE > 0) then
      -- en mode debug uniquement, touches spéciales pour tweaker la partie
      if (pKey == settings.appKeys.nextLevel) then nextLevel(); return end
      if (pKey == settings.appKeys.loseLife) then player.loseLife(true, true); return end
      if (pKey == settings.appKeys.loseGame) then screenLose.show(); return end
      if (pKey == settings.appKeys.winGame) then  screenWin.show(); return end
      if (pKey == settings.appKeys.nextPower) then
        debugPowerUpIndex = debugPowerUpIndex + 1
        if (debugPowerUpIndex > #debugPowerUpList) then debugPowerUpIndex = 1 end
        player.status = debugPowerUpList[debugPowerUpIndex]
        debugMessage("POWERUP player:"..player.status)
      end
    end -- if (DEBUG_MODE > 0) then

    if (pKey == settings.appKeys.pauseGame) then screenPause.show() end

    -- NOTE: le deplacement du joueur avec les touches se fait dans player.move()
    player.keypressed(pKey, pScancode, pIsrepeat)
    -- oblige le joueur à relacher des touches prédéfinies après une action donnée
    -- if (player.get_keysToRelease() == nil or player.get_keyToRelease(pKey) == nil or player.get_keyToRelease(pKey) == false) then
    -- player.keypressed(pKey, pScancode, pIsrepeat)
    -- end
  end -- screen.keypressed

  --[[ à décommenter pour des comportements spécifiques
  --- Gestion du "mousepressed" sur l'écran
  -- @param TODO: description
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screenPlay.mousepressed")
    -- rien à faire pour le moment
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param TODO: description
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screenPlay.mousemoved ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end -- screen.mousemoved
  ]]

  return lScreen
end -- createScreenPlay