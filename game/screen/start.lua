-- Entité représentant l'écran de Démarrage
-- Utilise une forme d'héritage simple: screenStart qui inclus screen
-- ****************************************
SCREEN_START = "screenStart"

--- Création de l'écran
-- @return un pseudo objet screenStart
function createScreenStart (pName, pBgImage, pMusic)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreenStart", pName, pBgImage, pMusic)

  local lScreen = createScreen(SCREEN_START, pBgImage, pMusic)

  -- liste des entrées de menu figurant sur l'écran
  lScreen.menuEntries = {
    {['text'] = 'Jouer', ['action'] = screenPlay.show},
    {['text'] = 'Scores', ['action'] = screenStats.show},
    {['text'] = 'Quitter', ['action'] = quitGame}
  }

  -- Override des fonctions de l'écran
  -- ******************************
  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  lScreen.screenInitialize= lScreen.initialize -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.initialize (pBgImage, pMusic)
    debugFunctionEnter("screenStart.initialize ", pBgImage, pMusic)
    lScreen.screenInitialize(pBgImage, pMusic)

    appState.initialize(20) -- niveau 20 pour gagner
    appState.hasStarted = true
  end -- screen.initialize

  --[[ à décommenter pour des comportements spécifiques
  --- Actualise l'écran
  -- @param pDt delta time
  lScreen.screenUpdate= lScreen.update -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screenStart.update") -- ATTENTION cet appel peut remplir le log
    lScreen.screenUpdate(pDt)
  end -- screen.update

  --- affiche l'écran
  lScreen.screenShow = lScreen.show -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.show (pHasWon)
    debugFunctionEnter("screenStart.show")
    lScreen.screenShow()
  end -- screen.show
  ]]

  --- Dessine l'écran
  lScreen.screenDraw = lScreen.draw -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screenStart.draw") -- ATTENTION cet appel peut remplir le log
    lScreen.screenDraw()
    love.graphics.push()
    love.graphics.scale(DISPLAY_SCALE_X, DISPLAY_SCALE_X)

    local w, h, font, i
    local color, content
    font = HUD.get_fontMenuContent()
    w = font:getHeight(" ")
    h = font:getHeight(" ")
    local offsetX = 0
    local offsetY = -h * 10

    color = {255, 160, 0, 255} -- couleur vert sans transparence
    font = HUD.get_fontMenuTitle()
    -- rappel: lHUD.animText (pText, pFont, pTextAnim, pOffsetX, pOffsetY, pWidth, pHeight, pColors, pTimerAnimIndex, pSpeed, pXstart, pYstart)
    HUD.animText(love.window.getTitle(), font, TEXT_ANIM_SINUS_CENTER, 0, offsetY, nil, nil, {color}, 1, 20)

    font = HUD.get_fontMenuContent()
    offsetY = 0

    lScreen.menuDraw(offsetX, offsetY, font, {255, 255, 0, 255})

    content = "Détruisez tous vos ennemis et restez en vie"
    HUD.displayTextScale(content, font, POS_CENTER, 0, offsetY + h * 5)

    content = "Atteignez le niveau "..appState.levelToWin.." pour gagner"
    HUD.displayTextScale(content, font, POS_CENTER, 0, offsetY + h * 6)

    content = "Q et D ou bien Droite et Gauche pour faire pivoter le vaisseau"
    HUD.displayTextScale(content, font, POS_CENTER, 0, offsetY + h * 8)

    content = "Z et S ou bien Haut et Bas pour accélérer et ralentir"
    HUD.displayTextScale(content, font, POS_CENTER, 0, offsetY + h * 9)

    content = "Espace pour tirer"
    HUD.displayTextScale(content, font, POS_CENTER, 0, offsetY + h * 10)

    content = "CTRL Gauche pour se téléporter"
    HUD.displayTextScale(content, font, POS_CENTER, 0, offsetY + h * 11)

    content = "Appuyer sur ESC pour quitter"
    HUD.displayTextScale(content, font, POS_CENTER, 0, offsetY + h * 12)

    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.pop()
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param TODO: description
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screenStart.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    lScreen.menuNavigation(pKey)
  end -- screen.keypressed


  --[[ à décommenter pour des comportements spécifiques
  --- Gestion du "mousepressed" sur l'écran
  -- @param TODO: description
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screenStart.mousepressed")
    screenPlay.show()
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param TODO: description
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screenStart.mousemoved",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end -- screen.mousemoved
  ]]

  return lScreen
end -- createScreenStart