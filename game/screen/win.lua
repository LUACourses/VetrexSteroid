-- Entité représentant l'écran de fin (partie gagnée)
-- Utilise une forme d'héritage simple: screenWin qui inclus screen
-- ****************************************
-- TYPE D'ECRAN
SCREEN_WIN = "screenWin"
-- TODO: éviter la redondance avec l'écran SCREEN_LOSE
--- Création de l'écran
-- @param pName nom de l'écran. Si absent, la valeur SCREEN_UNDEFINED sera utilisée
-- @param pBgImage (OPTIONNEL) image de fond.
-- @param pMusic (OPTIONNEL) musique
-- @return un pseudo objet screenWin
function createScreenWin (pName, pBgImage, pMusic)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreenWin", pName, pBgImage, pMusic)

  local lScreen = createScreen(SCREEN_WIN, pBgImage, pMusic)

  -- liste des entrées de menu figurant sur l'écran
  lScreen.menuEntries = {
    {['text'] = 'Rejouer', ['action'] = screenPlay.show},
    {['text'] = 'Scores', ['action'] = screenStats.show},
    {['text'] = 'Retour au menu', ['action'] = screenStart.show},
    {['text'] = 'Quitter', ['action'] = quitGame}
  }

  -- Override des fonctions de l'écran
  -- ******************************
  --[[ à décommenter pour des comportements spécifiques
  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  lScreen.screenInitialize= lScreen.initialize -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.initialize (pBgImage, pMusic)
    debugFunctionEnter("screenWin.initialize ", pBgImage, pMusic)
    lScreen.screenInitialize(pBgImage, pMusic)

    lScreen.isInitialized = true
  end -- screen.initialize

  --- Actualise l'écran
  -- @param pDt delta time
  lScreen.screenUpdate= lScreen.update -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screenWin.update") -- ATTENTION cet appel peut remplir le log
    lScreen.screenUpdate(pDt)
  end -- screen.update

  --- affiche l'écran
  lScreen.screenShow = lScreen.show -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.show ()
    debugFunctionEnter("screenWin.show")
    lScreen.screenShow()
  end -- screen.show
  ]]

  --- Dessine l'écran
  lScreen.screenDraw = lScreen.draw -- mémorise la fonction du parent (screen) afin de l'appeler si nécessaire
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screenWin.draw") -- ATTENTION cet appel peut remplir le log
    lScreen.screenDraw()
    love.graphics.push()
    love.graphics.scale(DISPLAY_SCALE_X, DISPLAY_SCALE_X)

    local w, h, font, i
    local color, content
    font = HUD.get_fontMenuContent()
    w = font:getHeight(" ")
    h = font:getHeight(" ")
    local offsetX = 0
    local offsetY = 0

    content = "Bravo,partie gagnee !"
    color = {0, 200, 0, 255} -- couleur vert sans transparence

    font = HUD.get_fontMenuTitle()
    textWidth = font:getWidth(content)
    -- rappel: lHUD.animText (pText, pFont, pTextAnim, pOffsetX, pOffsetY, pWidth, pHeight, pColors, pTimerAnimIndex, pSpeed, pXstart, pYstart)
    HUD.animText(content, font, TEXT_ANIM_SCROLL_HORZ, 0, 0, nil, nil, {color}, 1, 200, - textWidth, nil)

    offsetY = 200
    font = HUD.get_fontMenuContent()
    w = font:getHeight(" ")
    h = font:getHeight(" ")
    player.score = math.floor(player.score)
    HUD.displayTextScale("Votre score est de "..player.score, font, POS_CENTER, 0, offsetY)

    if (gameStats.isHighscore(player.score)) then
      -- TODO: remplacer les printf par des HUD.displayTextScale
      local width = viewport.getWidth()
      offsetY = offsetY + h * 2
      love.graphics.printf('Vous avez un HIGH SCORE!', 0, offsetY, width, 'center')
      offsetY = offsetY + h
      love.graphics.printf('Entrer votre nom:',0, offsetY, width, 'center')
      offsetY = offsetY + h
      color = {0, 200, 0, 255} -- couleur vert sans transparence
      love.graphics.setColor(color)
      love.graphics.printf(player.nickName,0, offsetY, width, 'center')
    end

    offsetY = offsetY + h * 2
    lScreen.menuDraw(offsetX, offsetY, font, color)

    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.pop()
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param TODO: description
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screenStart.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log

    if (gameStats.isHighscore(player.score)) then
      if (pKey == settings.appKeys.inputChange) then
        if (#player.nickName > 1) then
          player.nickName = player.nickName:sub(1, #player.nickName - 1)
        else
          player.nickName = ""
        end
      elseif (pKey == settings.appKeys.inputValid and #player.nickName > 0) then
        -- enregistre le score avec le nickname
        gameStats.addScore(player.score, player.nickName)
        gameStats.saveScore()
        -- retour au menu
        screenStart.show()
        return
      end
    end --  if (gameStats.isHighscore(player.score)) then

    lScreen.menuNavigation(pKey)

  end -- screen.keypressed

  --[[ à décommenter pour des comportements spécifiques
  --- Gestion du "mousepressed" sur l'écran
  -- @param TODO: description
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screenWin.mousepressed")
    -- rien à faire pour le moment
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param TODO: description
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screenWin.mousemoved ",pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end -- screen.mousemoved
  ]]

  --- Gestion de la saisie de texte sur l'écran
  -- @param pText texte
  function lScreen.textinput (pText)
    if (#player.nickName < 20) then
      player.nickName = player.nickName..pText
    end
  end -- screen.textinput

  return lScreen
end -- createScreenWin