-- Entité représentant l'état du jeu à un instant donné
-- ****************************************

--- Crée un pseudo objet de type appState
-- @return un pseudo objet appState
function newAppState ()
  local lState = {}

  --- Initialise l'objet
  -- @param pLevelToWin (OPTIONNEL) niveau à atteindre pour gagner le jeu
  -- @param pPointForLevel (OPTIONNEL) nombre de points à marquer pour passer un niveau
  function lState.initialize (pLevelToWin, pPointForLevel)
    debugFunctionEnter("appState.initialize ", pLevelToWin, pPointForLevel)
    if (pLevelToWin == nil) then pLevelToWin = 1 end
    if (pPointForLevel == nil) then pPointForLevel = 10 end

    -- Écran en cours d'affichage
    lState.currentScreen = nil
    -- le jeu a t-elle démarrée ?
    lState.hasStarted = false
    -- nombre de niveaux à atteindre pour gagner le jeu
    lState.levelToWin = pLevelToWin
    -- nombre de points à marquer pour passer un niveau
    lState.pointForLevel = pPointForLevel
    -- id du joueur courant
    lState.currentPlayerId = 1 -- TODO à utiliser pour le mode multi-joueur
    -- mémorisation d'états
    lState.storeState = {}
  end -- appState.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  -- Autres fonctions
  -- ******************************

  --- Détruit l'objet
  function lState.destroy ()
    debugFunctionEnter("appState.destroy")
    lState.clean()
    lState = nil
  end -- appState.destroy

  --- Effectue du nettoyage lié à l'objet en quittant le jeu
  function lState.clean ()
    -- pour le moment rien à faire
  end -- appstate.clean

  --- Enregistre l'état d'un joueur pour pouvoir le restituer plus tard
  -- @param pID (OPTIONNEL) index de l'objet player dans la table entities.characters. Si absent, la valeur 1 sera utilisée. Cela correspond au joueur courant
  function lState.backupPlayer (pId)
    if(pId == nil) then pId = 1 end
    if (entities.characters[pId] ~= nil and lState.storeState ~= nil) then
      lState.storeState.player = {}
      lState.storeState.player[pId] = {
        score = entities.characters[pId].score,
        level = entities.characters[pId].level,
        life = entities.characters[pId].life
      }
    end
  end -- appstate.backupPlayer

  --- Restaure l'état d'un joueur
  -- @param pID (OPTIONNEL) index de l'objet player dans la table entities.characters. Si absent, la valeur 1 sera utilisée. Cela correspond au joueur courant
  -- @return l'objet player correspondant à celui de la session courante
  function lState.restorePlayer (pId)
    if(pId == nil) then pId = 1 end
    local pPlayer = entities.characters[pId]
    if (entities.characters[pId] ~= nil and lState.storeState.player ~= nil and lState.storeState.player[pId] ~= nil) then
      entities.characters[pId].score = lState.storeState.player[pId].score
      entities.characters[pId].level = lState.storeState.player[pId].level
      entities.characters[pId].life = lState.storeState.player[pId].life
    end
    return pPlayer
  end -- appstate.restorePlayer

  -- initialisation par défaut
  lState.initialize()
  return lState
end -- newAppState