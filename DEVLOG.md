# PRESENTATION
======

## le projet
VetrexSteroid

## L'idée

Remake du jeu "Mine Storm", paru sur la console Vetrex dans les années 80.
Ce jeu est lui-même une adaptation du célèbre "asteroid".

## Contraintes :

Ce projet est réalisé dans la cadre de la GameCodeur JAM#11.
Le thème de la Jam est "Pure Code".
Donc pas de possibilité d'utiliser des images.
Au départ, le thème ne pas pas emballé plus que ça, mais j'ai assez rapidement pensé à la console Vetrex que j'avais beaucoup aimé dans mon enfance, et je me suis dit que développer un jeu sur la base des graphisme vectoriels de l'époque serait plutôt fun.

## Langage/Framework :

Lua/Love2D  Librairies externes : AUCUNE de prévue

L'idée est de coder tout par moi-même , en utilisant le moins d'éléments externes ou pré-fabriqués. Je vais utiliser Love2D_Skeleton,  un FrameWork  Love2D perso, que j'améliore au fur et à mesure de mon avancée de son utilisation dans mes projets.

Pour les intéressés et les curieux, ce framework est téléchargeable gratuitement à l'adresse:  https://gitlab.com/LUACourses/Love2D_Skeleton

# A:GRANDES LIGNES DU PROJET
======

## infos préliminaires

- utilisation d'un fichier texte (celui là) ou d'une page Web (devlog sur gitlab ?) pour afficher et donner des infos pendant le live Coding
- utilisation de commentaires et de pseudo code dans les fichiers sources pour préciser les actions effectuées
- mon poste de travail comporte 3 écrans, seul 1 écran est monitoré pour la diffusion en live, toutes les manipulation ne seront donc pas visibles dans la vidéos du live coding
- je vais créer un dépôt sur gitlab qui proposera les sources du projet en téléchargement libre.

## Mise en place du dépôt git

## ajout du jeu dans les prototypes

## suppression des images et remplacement par des vecteurs
- OK:étudier rapidement la faisabilité d'une génération dynamique en fonction d'une liste de points reliés par des lignes (cf code convexHull)
- BONUS:gérer les destructions en plusieurs morceaux plus petits

## ajouter des effets
- BONUS: ajouter des animations
- OK:gérer les explosions

## intégration du starfield
-  OK: reprendre le code existant (cf dossier gamecodeur_starfield)

## personnalisation des ennemis
- OK: creation des ennemis en fonction du gameplay original (cf Wikipedia et description du gameplay)
- BONUS: intégration d'un ennemi avec de l'IA

## personnalisation des sons
- OK: voir dans les assets dispo et libres de droits
- BONUS:sons générés procéduralement ?

## personnalisation des musiques
- OK:voir dans les assets dispo et libres de droits
- BONUS:musiques générées procéduralement ?

## ajout des options
- BONUS:tableau de hight scores

# B:IMPLEMENTATION
======
## Mise en place du dépôt git
- OK dépôt créé : git@gitlab.com:LUACourses/VetrexSteroid.git
- OK récupération des sources du framework Love2D_Skeleton
- OK 1er commit dans le dépôt

## ajout du jeu dans les prototypes
- OK création des constantes liées au nouveau "prototype" de jeu
- OK copie des assets existants pour un test rapide
- OK nettoyage du code pour enlever les éléments superflus. NOTE: tout le code désactivé sera mis en commentaire avec la balise GCJAM11. il pourra être supprimé à la fin
  - OK remplacement du code de la map par la gestion du starfield :
	- OK suppression des images des tuiles et de définition des niveaux
	- OK modification du code de chargement de la map
- OK commit dans le dépôt

## suppression des images et remplacement par des vecteurs

### remplacer les images du joueur
- OK: vaisseau
	- OK le plus simple est de remplacer le chargement d'un fichier image dans le resourceMAnager par le chargement d'un fichier text contenant la liste des points à dessiner
		- OK ajouter un calcul pour sprite.width et sprite.height quand ce ne sont pas de vraies images
	- OK ajouter les coordonnées du jouer à chaque point
	- OK calculer la rotation des points en fonction de l'angle de rotation
		- OK modifier le mode de déplacement du joueur (rotation + speed) au lieu du mouvement direct
		- OK trouver comment appliquer une rotation à une série de points
		- OK corriger le bug sur le positionnement de la collision box suite au décalage de l'origine du joueur

- OK: projectiles
	- OK remplacer les images
	- OK tirer dans la direction de la rotation du joueur

### modifier/évaluer les prototypes des npc (vaisseaux et "mines")

#### remplacer les images des npc
- OK vaisseau
	- BOF générer des polygones aléatoires ?
		- BOF créer des polygones convexes à partir de points aléatoires
			- BOF utiliser les "convexHULL" (cf sources)
				- résultat insatisfaisant car il ne produit que "l'entourage" des points
			- BOF fermer juste le polygone
				- résultat insatisfaisant
	- BOF placer les points manuellement pour chaque vaisseau
		- BOF , trop long a prendre en main : chercher un outil générant des coordonnées à partir d'un dessin
	- OK faire comme avec le joueur
		- OK faire de même avec tous les npc
			-- OK enlever l'affichage des "boucliers" (ajouter un attribut dans le sprite)
- OK projectiles

### remplacer les effets et UI
- OK explosions
	- OK remplacer les effets par des images comme pour les autres sprites ou bien utiliser des formes simples
		- OK: remplacer les effets par des images comme pour les autres sprites


### remplacer les images du joueur (suite)
- OK: modifier les animations existantes.

### mettre en ligne une première version
- OK préparer le code pour un premier prototype

## GAMEPLAY
- OK modifier la forme des ennemis en s'inspirant du jeu initial
- OK comment sont spawnés les ennemis ?
	- OK définir les conditions de victoire (nombre de niveau à finir ?)
		- OK: définir le niveau max pour la fin du jeu (atteindre le niveau 20)
	- définir les conditions de passage d'un niveau: Nb d'ennemis détruits ou tuer le boss
		- définir un nombre d'ennemis à détruire en fonction du niveau du joueur: ENEMIES_TO_DESTROY
		- définir un nombre d'ennemis détruits : ENEMIES_DESTROYED
		- OK: tenir un compte des ennemis présents à l'écran
		- définir la répartition des ennemis en fonction du niveau courant
			- faire apparaître aléatoirement un boss qui met fin au niveau (?)

		- pseudo-code de spawn
			- définir un Nb max d'ennemis  à l'écran : ENEMIES_SHOWN_MAX
			- compter les ennemis à l'écran : ENEMIES_COUNT
			- définir la probabilité de spawner un ennemi: ENEMIS_SPAWN_PROBABILITY (entre 0 et 1, dépend du niveau),
				- si ENEMIES_COUNT < ENEMIES_SHOWN_MAX et rand(0,1)<ENEMIS_SPAWN_PROBABILITY alors spawner un ennemi
			- spawner un ennemi
				- définir une position en dehors de l'écran pour que l'ennemi apparaisse graduellement
				- définir un type d'ennemi parmi ceux possibles en fonction du niveau de jeu courant
			- tester si le niveau est fini
				- si le boss est détruit (OK, c'est automatique actuellement)
				- si le nombre d'ennemis à détruire est atteint
					- si ENEMIES_DESTROYED>=ENEMIES_TO_DESTROY alors changer de niveau

- OK: améliorer la maniabilité du joueur
	- OK:ajouter un effet de recul à chaque tir

- OK ajouter un léger déplacement horizontal à la trajectoire du npcfall

- OK modifier la police d'affichage
	- tester différentes polices
		- OK utilisation de  failed attempt
		- OK agrandir la taille

- OK améliorer le gameplay
	- OK modifier le spawn
		- OK créer un npcbounce au début et à la fin de la liste
		- OK faire tirer les npcbounce
		- OK ralentir les npcbounce
		- OK diminuer l'effet de recul du joueur
		- OK spawner un ennemi dès le départ

- OK ajouter le starfield
- OK ajouter la possibilité de se téléporter (cf gameplay original)

- OK ajouter un texte explicatif sur les objectifs en début de partie
	- changer la police de texte de la page d'accueil

- OK: ajouter un tableau des meilleurs scores
- OK: intégrer les musiques des menus au pseudo objets "screen" + chargement
- OK: intégrer les images de fond des menus au pseudo objets "screen" + chargement

## FINALISATION
- OK personnaliser les différentes explosions (actuellement même fichiers dupliqués)
- OK finaliser tester les écrans start, pause, end
- OK personnaliser les sons et les musiques

# pour chaque release
- supprimer les "white spaces"
- corriger l'orthographe
- réactiver l'écran de départ
- enlever le debug
- créer les sources
- créer l'exécutable