## Description
Remake du jeu "Mine Storm", paru sur la console Vetrex dans les années 80.
Ce jeu est lui-même une adaptation du célèbre "asteroid". 

Ce projet est réalisé dans la cadre de la GameCodeur JAM#11.
Le thème de la Jam est "Pure Code".
Donc pas de possibilité d'utiliser des images. 

===
### Objectifs
Détruire tous les ennemis qui apparaissent successivement dans le niveau pour passer au niveau suivant.  
Eviter les ennemis et leurs projectiles.  
Chaque type d'ennemis a un comportement spécifique qu'il sera utile de reconnaître.  

Pour gagner la partie, le joueur doit terminer les 20 niveaux du jeu.  

### Mouvements et actions du joueur
Faire pivoter le vaisseau: touches Q et D ou bien les flèches Droite et Gauche.  
Accélérer et ralentir: touches Z et S ou bien les flèches Haut et Bas.  
Tirer: Touche "espace".  
Se téléporter à une autre endroit de la zone de jeu: touche "contrôle gauche".  
Mettre en pause: touche P. 
Relancer la partie: touche R.  
Musique précédente: touche F1.  
Musique suivante: touche F2.  
Quitter le jeu: clic sur la croix ou touche escape.  

#### en mode debug uniquement 
Basculer sur un autre prototype de jeu et relancer le jeu: touche F8.  
Activer le mode Debug en live (ON/OFF): touche F9.  
Activer les déplacements avec le clavier (ON/OFF): touche F10.  
Activer les déplacements avec la souris (ON/OFF): touche F11.  
Confiner la souris dans la fenêtre (ON/OFF): touche F12.  
Passer au niveau suivant: touche + (pavé numérique).  
Perdre une vie: touche - (pavé numérique).  
Utiliser le powerUp suivant: touche * (pavé numérique). 
Perdre la partie: touche Fin.  
Gagner la partie: touche Debut.  

### Interactions
- Le joueur:  
  + peut se déplacer.  
  + peut tirer.  
  + peut détruire les ennemis.  
  + peut être détruit par les ennemis ou leur projectiles.  
  + ne peut pas sortir de la zone de jeu.  
- les ennemis:  
  + peuvent se déplacer.  
  + peuvent tirer.  
  + peuvent détruire le joueur.  
  + peuvent être détruits par le joueur ou ses projectiles.  

### Copyrights
Ecrit en LUA et en utilisant le Framework Love2D.  
Utilise le FrameWork Love2D_skeleton, disponible à l'adresse https://gamecoderblog.itch.io/love2d-skeleton

Développé sans outil spécifique, en utilisant principalement SublimeText 3 et ZeroBrane Studio (pour le débuggage).  

-----------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)

=================================================

### Description
Remake of the game "MineStorm", released on the Vetrex console in the 80's.
This game is itself an adaptation of the famous "asteroid".

This project is made for the GameCodeur JAM #11.
The theme of the Jam is "Pure Code".
So no possibility to use images.

===
### Goals
Destroy all enemies that appear successively in the level to go to the next level.
Avoid the enemies and their projectiles.
Each type of enemy has a specific behavior that will be useful to recognize.

To win the game, the player must complete all the 20 levels of the game.

### Movements and actions of the player
Rotate the ship: Q and D keys or right and left arrows.
Accelerate and slow down: Z and S keys or up and down arrows.
Fire: The "Space" key.
Teleport to another location in the playing area: The "left control" key.
Pause: P key.  
Restart the game: R key.  
Previous music: F1 key.  
Next music: F2 key.  
Exit the game: click on the cross or escape key.  

### debug mode only
Switch to another game prototype et restart the game: F8 key.  
Activate the Live Debug mode (ON/OFF): F9 key.  
Enable the movements with the keyboard (ON/OFF): F10 key.  
Enable the movements with the mouse (ON/OFF): F11 key.  
Confining the mouse in the window (ON/OFF): F12 key.  
Go to the next Level: + key (keypad).  
Lose a life: - key (keypad).  
Use the next powerUp: * key (keypad).  
Lose the game: End key.  
Win the game: Home key.  

### Interactions
- The player:  
   + can move.  
   + can fire.  
   + can destroy the enemies.  
   + can be destroyed by the enemies or their projectiles.  
   + can not get out of the play area.  
- The enemies:    
   + can move.  
   + can shoot.  
   + can destroy the player.  
   + may be destroyed by the player or his projectiles.  

### Copyrights
Written in LUA and with the Love2D Framework.  
Uses the FrameWork Love2D_skeleton, available at https://gamecoderblog.itch.io/love2d-skeleton

Developed without any specific tool, mainly using SublimeText 3 and ZeroBrane Studio (for debugging).  

------------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)