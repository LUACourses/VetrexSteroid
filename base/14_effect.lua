-- Entité représentant un effet visuel
-- Utilise une forme d'héritage simpSle: effect qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
SPRITE_TYPE_EFFECT = SPRITE_TYPE..SPRITE_EXT_EFFECT -- un effet visuel
SPRITE_TYPE_EFFECT_EXPLODE = SPRITE_TYPE_EFFECT..NEXT_EXT_EXPLODE -- une explosion
SPRITE_TYPE_EFFECT_EXPLODE_PROJ = SPRITE_TYPE_EFFECT_EXPLODE..NEXT_EXT_PROJ -- une explosion de projectile
SPRITE_TYPE_EFFECT_EXPLODE_PLAYER = SPRITE_TYPE_EFFECT_EXPLODE..NEXT_EXT_PLAYER -- une explosion de joueur
SPRITE_TYPE_EFFECT_EXPLODE_NPC = SPRITE_TYPE_EFFECT_EXPLODE..NEXT_EXT_NPC -- une explosion de pnj
SPRITE_TYPE_EFFECT_EXPLODE_BOSS = SPRITE_TYPE_EFFECT_EXPLODE..NEXT_EXT_BOSS -- une explosion de boss

--- Création d'un effet visuel (EFFECT)
-- @param pType (OPTIONNEL) type d'effet (voir les constantes SPRITE_TYPE_EFFECT_xxx). Si absent la valeur SPRITE_TYPE_EFFECT_EXPLODE sera utilisée.
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur player.x sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur player.y sera utilisée.
-- @param pWidth (OPTIONNEL) largeur (en pixel). Si absent la valeur player.width / 2 sera utilisée.
-- @param pHeight (OPTIONNEL) hauteur (en pixel). Si absent la valeur player.height / 2 sera utilisée.
-- @return un pseudo objet effect
function createEffect (pType, pX, pY, pWidth, pHeight)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createEffect ", pType, pX, pY, pWidth, pHeight)

  if (pType == nil) then pType = SPRITE_TYPE_EFFECT_EXPLODE end
  if (pX == nil) then pX = player.x end
  if (pY == nil) then pY = player.y end
  if (pWidth == nil) then pWidth = (player.width / 2) or 15 end
  if (pHeight == nil) then pHeight = (player.height / 2) or 15 end

  local lEffect = {}

  local index

  -- rappel: createSprite(pImage, pX,pY,pvX,pvY,pType,pWeight,pLife,pEnergy,pPoints,pCanCollideWithTile,pVelocityMax,pVelocityJump,pGravity,pCanMakeAction,pStatus,pMustDestroyedAtBottom,pTimeToLive,pMustDestroyedOnCollision,pSpeed,pMustRespawnIfDestroyed)
  -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
  if (pType == SPRITE_TYPE_EFFECT_EXPLODE) then
    -- explosion de base
    lEffect = createSprite(FOLDER_EFFECTS_ANIMS.."/explosion/explode1.ptxt", pX, pY, 0, 0 ,pType, 0, 1, 1, 0, false, -1, 0, 0, false, SPRITE_STATUS_DESTROYING, true)
    lEffect.addAnimation(ANIM_NAME_STATE_DESTROY, FOLDER_EFFECTS_ANIMS.."/explosion", ".ptxt", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_DESTROYING, lEffect.clean)

  elseif (pType == SPRITE_TYPE_EFFECT_EXPLODE_PROJ) then
    -- explosion d'un projectile
    lEffect = createSprite(FOLDER_EFFECTS_ANIMS.."/explosion_projectile/explode1.ptxt", pX, pY, 0, 0 ,pType, 0, 1, 1, 0, false, -1, 0, 0, false, SPRITE_STATUS_DESTROYING, true)
    lEffect.addAnimation(ANIM_NAME_STATE_DESTROY, FOLDER_EFFECTS_ANIMS.."/explosion_projectile", ".ptxt", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_DESTROYING, lEffect.clean)
  elseif (pType == SPRITE_TYPE_EFFECT_EXPLODE_PLAYER) then
    -- explosion du joueur
    -- on crée une grosse explosion (entre 3 et 6 "petites" explosions)
    for index = 1, math.random(3, 6) do
      lEffect = createSprite(FOLDER_EFFECTS_ANIMS.."/explosion_player/explode1.ptxt", pX + math.random(-pWidth, pWidth) , pY + math.random(-pHeight, pHeight), 0, 0 ,pType, 0, 1, 1, 0, false, -1, 0, 0, false, SPRITE_STATUS_DESTROYING, true)
      lEffect.addAnimation(ANIM_NAME_STATE_DESTROY, FOLDER_EFFECTS_ANIMS.."/explosion_player", ".ptxt", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_DESTROYING, lEffect.clean)
    end
  elseif (pType == SPRITE_TYPE_EFFECT_EXPLODE_NPC) then
    -- explosion d'un PNJ
    -- on crée une grosse explosion (entre 2 et 4 "petites" explosions)
    for index = 1, math.random(2, 4) do
      lEffect = createSprite(FOLDER_EFFECTS_ANIMS.."/explosion_npc/explode1.ptxt", pX + math.random(-pWidth, pWidth) , pY + math.random(-pHeight, pHeight), 0, 0 ,pType, 0, 1, 1, 0, false, -1, 0, 0, false, SPRITE_STATUS_DESTROYING, true)
      lEffect.addAnimation(ANIM_NAME_STATE_DESTROY, FOLDER_EFFECTS_ANIMS.."/explosion_npc", ".ptxt", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_DESTROYING, lEffect.clean)
    end
  elseif (pType == SPRITE_TYPE_EFFECT_EXPLODE_BOSS) then
    -- explosion d'un PNJ Boss
    -- on crée une plus grosse explosion (entre 10 et 20 "petites" explosions)
    for index = 1, math.random(10, 20) do
      lEffect = createSprite(FOLDER_EFFECTS_ANIMS.."/explosion_boss/explode1.ptxt", pX + math.random(-pWidth, pWidth) , pY + math.random(-pHeight, pHeight), 0, 0 ,pType, 0, 1, 1, 0, false, -1, 0, 0, false, SPRITE_STATUS_DESTROYING, true)
      lEffect.addAnimation(ANIM_NAME_STATE_DESTROY, FOLDER_EFFECTS_ANIMS.."/explosion_boss", ".ptxt", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_DESTROYING, lEffect.clean)
    end
    -- and so on ...
  end

  --- Initialise l'objet
  function lEffect.initialize ()
    debugFunctionEnter("effect.initialize")

    lEffect.lineWidth = 1

    -- définit les hotSpots pour les collisions
    -- pas de collisions pour le effets
    lEffect.createCollisionBox(COLLISION_MODEL_NONE)

    lEffect.updateInitialValues() -- important
  end -- effect.initialize

  lEffect.initialize()
  lEffect.destroy() -- important pour que le sprite soit détruit automatiquement à la fin de son animation
  return lEffect
end -- createEffect