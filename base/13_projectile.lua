-- Entité représentant un projectile
-- Utilise une forme d'héritage simpSle: projectile qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
SPRITE_TYPE_PROJ = SPRITE_TYPE..SPRITE_EXT_PROJ
SPRITE_TYPE_PROJ_PLAYER = SPRITE_TYPE_PROJ..NEXT_EXT_PLAYER -- un projectile de joueur
SPRITE_TYPE_PROJ_NPC = SPRITE_TYPE_PROJ..NEXT_EXT_NPC -- un projectile de pnj

--- Création d'un projectile
-- @param pType (OPTIONNEL) type de projectile (voir les constantes SPRITE_TYPE_PROJ_XXX). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur "" sera utilisée.
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur 1 sera utilisée.
-- @param pDamages (OPTIONNEL) dommages infligés. Si absent la valeur 1 sera utilisée.
-- @param pSpeed (OPTIONNEL) vitesse.  Si absent la valeur par défaut des sprites sera utilisée.
-- @return un pseudo objet projectile
function createProjectile (pType, pImage, pX, pY, pLife, pEnergy, pDamages, pSpeed)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  ---- debugFunctionEnterNL("createProjectile ", pType, pImage, pX, pY, pLife, pDamages, pSpeed) -- ATTENTION cet appel peut remplir le log
  if (pDamages == nil) then pDamages = 1 end

  -- rappel:          createSprite(pImage,pX, pY ,pvX ,pvY , pType,pWeight, pLife, pEnergy,pPoints,pCanCollideWithTile,pVelocityMax,pVelocityJump,pGravity,pCanMakeAction,pStatus              ,pMustDestroyedAtBorder,pTimeToLive,pMustDestroyedOnCollision, pSpeed)
  local lProjectile = createSprite(pImage, pX, pY, nil, nil, pType, 1     , pLife, pEnergy, 0     , false             , -1         , 0           , 0      , false        , SPRITE_STATUS_NORMAL, true                 , -1        , true                    , pSpeed) -- poids non nul sinon le projectile ne se déplace pas en Y par défaut

  -- attributs spécifiques au projectile
  -- ******************************
  -- dommages infligés
  lProjectile.damages = pDamages

  --- Initialise le projectile
  function lProjectile.initialize ()
    debugFunctionEnter("projectile.initialize")

    -- modification de la vitesse de déplacement en fonction du niveau du joueur
    lProjectile.speed = lProjectile.speed * getSpeedDifficultyFactor()

    -- définit les hotSpots pour les collisions
    lProjectile.createCollisionBox(COLLISION_MODEL_BOX4)

    lProjectile.updateInitialValues() -- important
  end -- projectile.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --[[ à décommenter pour des comportements spécifiques
  --- affiche le projectile
  lProjectile.spriteDraw = lProjectile.draw -- mémorise la fonction du parent (sprite) afin de l'appeler si nécessaire
  function lProjectile.draw ()
    ---- debugFunctionEnterNL("projectile.draw") -- ATTENTION  cet appel peut remplir le log
    -- affiche le projectile comme un sprite normal
     lProjectile.spriteDraw()
  end -- projectile.draw
  ]]

  --- Détruit le projectile
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, la valeur false sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lProjectile.spriteDestroy = lProjectile.destroy -- mémorise la fonction du parent (sprite) afin de l'appeler si nécessaire
  function lProjectile.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    ---- debugFunctionEnterNL("projectile.destroy ", pAddScore, pMustShowFX, pMustPlaySound) -- ATTENTION cet appel peut remplir le log
    -- ?? if (lProjectile.status ~= SPRITE_STATUS_NORMAL) then return end

    if (pMustAddScore == nil) then pMustAddScore = false end
    if (pMustShowFX == nil) then pMustShowFX = lProjectile.mustDestroyedWithFX end
    if (pMustPlaySound == nil) then pMustPlaySound = lProjectile.mustDestroyedWithSound end

    if (pMustAddScore) then
      player.addScore(lProjectile.points)
    end
    if (pMustShowFX) then
      createEffect(SPRITE_TYPE_EFFECT_EXPLODE_PROJ, lProjectile.x, lProjectile.y, lProjectile.width, lProjectile.height)
    end
    if (pMustPlaySound) then
      lProjectile.playSound("Destroy")
    end
    -- la destruction effective se fait dans la classe parent
    lProjectile.spriteDestroy()
  end -- projectile.destroy

  --[[ à décommenter pour des comportements spécifiques
  --- Actualise le projectile
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lProjectile.spriteUpdate = lProjectile.update
  function lProjectile.update (pDt)
    ---- debugFunctionEnterNL("projectile.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lProjectile.spriteUpdate(pDt)

    if (lProjectile.status == SPRITE_STATUS_DESTROYING or lProjectile.status == SPRITE_STATUS_DESTROYED) then return end

  end -- projectile.update
  ]]

  --- Vérifie les collisions du projectile (override)
  -- @return true en cas de collision, SPRITE_COLLISION_NONE sinon
  function lProjectile.collide ()
    ---- debugFunctionEnterNL("projectile.collide") -- ATTENTION cet appel peut remplir le log

    if (not lProjectile.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end
    local collisionType = SPRITE_COLLISION_NONE
    local spriteIsProjectilePlayer = (lProjectile.type:find(SPRITE_TYPE_PROJ_PLAYER) ~= nil)
    local spriteIsProjectilePnj = (lProjectile.type:find(SPRITE_TYPE_PROJ_NPC) ~= nil)
    local index, spriteIsNpc
    local xMin, yMin, xMax, yMax= lProjectile.getScreenLimits()
    if (spriteIsProjectilePlayer) then
      -- projectile du joueur
      -- touche t-il un PNJ ?
      -- boucle sur les sprite de type PNJ
      for index = #entities.sprites, 1, -1 do -- le parcours décroissant permet de gérer un table.remove dans une liste en cours de parcours
        lNPC = entities.sprites[index]
        spriteIsNpc = (lNPC.type:find(SPRITE_TYPE_NPC) ~= nil)
        -- le sprite est il un pnj ?
        if (spriteIsNpc) then
            -- le projectile touche t-il le pnj ?
            if (lProjectile.collideWithSprite(lNPC) ~= SPRITE_COLLISION_NONE) then
            -- oui
            -- le projectile perd une vie, et ne re-spawne pas
            lProjectile.loseEnergy(lNPC.energy, true, false)
            -- le PNJ perd de l'énergie, et ne re-spawne pas
            lNPC.loseEnergy(lProjectile.damages, false, false)
            collisionType = SPRITE_COLLISION_NPC
            break
          end
        end
      end
    elseif (spriteIsProjectilePnj) then
      -- projectile d'un PNJ
      -- le projectile touche t-il le joueur ?
      if (lProjectile.collideWithSprite(player) ~= SPRITE_COLLISION_NONE) then
        -- oui
        -- le projectile perd une vie, et ne re-spawne pas
        lProjectile.loseEnergy(player.energy, true, false)
        -- le joueur perd de l'énergie, et re-spawne
        player.loseEnergy(lProjectile.damages, false, true)
        collisionType = SPRITE_COLLISION_PLAYER
      end
    end

    -- vérifie si le projectile est sorti de l'écran et qu'il n'a pas déjà été supprimé lors d'une collision
    if ((lProjectile.x < xMin or lProjectile.x > xMax or lProjectile.y < yMin or lProjectile.y > yMax) and lProjectile. status ~= SPRITE_STATUS_DESTROYED) then
      -- oui, on le supprime
      lProjectile.destroy(false, false, false)
    end

    return collisionType
  end -- projectile.collide

  -- initialisation par défaut
  lProjectile.initialize()

  return lProjectile
end -- createProjectile
