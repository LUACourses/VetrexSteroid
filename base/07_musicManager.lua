-- Entité gérant les musiques
-- ****************************************

--- Crée un pseudo objet de type musicManager
-- @return un pseudo objet musicManager
function createMusicManager ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createMusicManager")

  local lMM = {}

  --- Initialise l'objet
  function lMM.initialize ()
    debugFunctionEnter("musicManager.initialize")

    -- pas pour l'augmentation automatique du volume
    lMM.stepUp    = 0.2 -- 1/5 de seconde
    -- pas pour la diminution automatique du volume
    lMM.stepDown  = 0.2 -- 1/5 de seconde
    -- index de la piste en cours
    lMM.trackPlayed   = 0 -- pas de morceau en cours
    -- une piste est elle en cours de lecture
    lMM.isPlaying = false
    -- liste des pistes (musiques) disponibles
    lMM.trackList = {}
  end -- musicManager.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  -- Autres fonctions
  -- ******************************

  --- Détruit l'objet
  function lMM.destroy ()
    debugFunctionEnter("musicManager.destroy")
    lMM.clean()
    lMM = nil
  end -- musicManager.

  --- Effectue des nettoyages en quittant le jeu
  function lMM.clean ()
    debugFunctionEnter("musicManager.clean")
    -- pour le moment rien à faire
  end -- musicManager.clean

  --- Met à jour les pistes à lire et applique un fading sur le volume si besoin
  -- @param pDt delta time
  function lMM.update (pDt)
    ---- debugFunctionEnterNL("musicManager.update ", pDt) -- ATTENTION cet appel peut remplir le log
    if (lMM.trackPlayed < 1 or #lMM.trackList < 1 or not lMM.isPlaying) then return end
    local i
    local debug = ""
    for i = 1, #lMM.trackList do
      -- augmente le volume de la piste en cours s'il n'est pas déjà au maximum
      local track = lMM.trackList[i]
      local volume = track.audioSource:getVolume()
      debug = debug .. "track "..i
      if (i == lMM.trackPlayed and volume < track.maximalVolume) then
        track.audioSource:setVolume(volume + lMM.stepUp * pDt)
        debug = debug .. " UP "..track.audioSource:getVolume()
      end
      -- diminue le volume des pistes autres que celle en cours si leur volume n'est pas déjà nul
      if (i ~= lMM.trackPlayed) then
        if (volume > 0) then
          -- baisse le volume progressivement
          track.audioSource:setVolume(volume - lMM.stepDown * pDt)
          debug = debug .. " DW "..track.audioSource:getVolume()
        else
          -- volume égal à 0, on arrête la lecture
          track.audioSource:stop()
        end
      end
    end
  end -- musicManager.update

  --- Ajoute une piste à la liste
  -- @param pFilename nom complet du fichier audio (relatif au dossier du jeu)
  -- @param pMode (OPTIONNEL) "static" ou "stream" (voir l'option associée de la fonction love.audio.newSource)
  -- @param pMaximalVolume (OPTIONNEL) volume maximal auquel il doit être joué (entre 0 et 1)
  -- @param pIsLooping (OPTIONNEL) true pour jouer la piste en boucle
  -- @param pNoFade (OPTIONNEL) true pour indiquer que cette piste sera toujours jouée immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  function lMM.add (pFilename, pMode, pMaximalVolume, pIsLooping, pNoFade)
    debugFunctionEnter("musicManager.add ", pFilename, pMode, pMaximalVolume, pIsLooping, pNoFade)
    if (assertEqual(pFilename, nil, "musicManager.addTrack:pFilename")) then return end
    if (pMode == nil) then pMode = "static" end
    if (pMaximalVolume == nil) then pMaximalVolume = 1 end
    if (pIsLooping == nil) then pIsLooping = true end
    if (pNoFade == nil) then pNoFade = false end

    local newTrack = {}
    newTrack.audioSource = resourceManager.getSound(pFilename, pMode)
    newTrack.mode = pMode
    newTrack.maximalVolume = pMaximalVolume
    newTrack.isLooping = pIsLooping
    newTrack.noFade = pNoFade
    lMM.trackList[#lMM.trackList + 1] = newTrack
  end -- musicManager.add

  --- Lit la piste demandée
  -- @parampTrackIndex index de la piste parmi celles ajoutées précédemment
  -- @param pVolume (OPTIONNEL) volume auquel la piste doit être jouée (entre 0 et 1). Si non nil, redéfini la valeur maximalVolume de la piste
  -- @param pNoFade (OPTIONNEL) true pour jouer la piste immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  function lMM.play (pTrackIndex, pVolume, pNoFade)
    debugFunctionEnter("musicManager.play ", pTrackIndex, pVolume, pNoFade)
    if (#lMM.trackList < 1) then return end

    if (pTrackIndex == nil) then pTrackIndex = lMM.trackPlayed end
    if (pTrackIndex > #lMM.trackList) then
      -- si l'index de la piste choisi dépasse celui du nombre de pistes, on joue une piste aléatoirement
      pTrackIndex = math.random(1, #lMM.trackList)
    end

    local track = lMM.trackList[pTrackIndex]
    if (pVolume ~= nil) then
      -- si le volume a été fixé, alors on change le volume maximal de la piste
      track.maximalVolume = pVolume
    end
    track.audioSource:setLooping(track.isLooping)
    if (pNoFade) then
      -- si la lecture est immédiate (nofade), on fixe le volume des pistes sans attendre l'update
      lMM.trackPlayed.audioSource:setVolume(0) -- courante à 0
      track.audioSource:setVolume(pVolume) -- nouvelle piste au volume max
    else
      -- sinon on débute avec un volume = 0
      track.audioSource:setVolume(0)
    end
    lMM.trackPlayed = pTrackIndex
    track.audioSource:play()

    lMM.isPlaying = true
  end -- musicManager.play

  --- Met la piste courante en pause
  function lMM.pause ()
    debugFunctionEnter("musicManager.pause")
    if (#lMM.trackList < 1 or lMM.trackPlayed < 1) then return end
    local track = lMM.trackList[lMM.trackPlayed]
    if (track ~= nil) then
      if (track.audioSource:isPlaying()) then
        track.audioSource:pause()
      end
    end
    lMM.isPlaying = false
  end -- musicManager.pause

  --- Reprend la lecture de la piste courante
  function lMM.resume ()
    debugFunctionEnter("musicManager.resume")
    if (#lMM.trackList < 1 or lMM.trackPlayed < 1) then return end
    local track = lMM.trackList[lMM.trackPlayed]
    if (track ~= nil) then
      if (not track.audioSource:isPlaying()) then
        track.audioSource:resume()
      end
    end
    lMM.isPlaying = true
  end -- musicManager.resume

  --- Arrête la lecture de la piste courante
  function lMM.stop ()
    debugFunctionEnter("musicManager.stop")
    if (#lMM.trackList < 1 or lMM.trackPlayed < 1) then return end
    local track = lMM.trackList[lMM.trackPlayed]
    if (track ~= nil) then
      track.audioSource:stop()
    end
    lMM.isPlaying = true
  end -- musicManager.stop

  --- Lit la piste suivante
  -- @paramtrackIndex index de la piste parmi celles ajoutées précédemment
  -- @param pVolume (OPTIONNEL) volume auquel la piste doit être jouée (entre 0 et 1). Si non nil, redéfini la valeur maximalVolume de la piste
  -- @param pNoFade (OPTIONNEL) true pour jouer la piste immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  function lMM.playNext (pVolume, pNoFade)
    debugFunctionEnter("musicManager.playNext ", pVolume, pNoFade)
    local index = lMM.trackPlayed + 1
    if (index > #lMM.trackList) then index = 1 end
    lMM.play(index, pVolume, pNoFade)
  end -- musicManager.playNext

  --- Lit la piste précédente
  -- @paramtrackIndex index de la piste parmi celles ajoutées précédemment
  -- @param pVolume (OPTIONNEL) volume auquel la piste doit être jouée (entre 0 et 1). Si non nil, redéfini la valeur maximalVolume de la piste
  -- @param pNoFade (OPTIONNEL) true pour jouer la piste immédiatement (sans effet de fading entre la nouvelle et l'ancienne piste)
  function lMM.playPrevious (pVolume, pNoFade)
    debugFunctionEnter("musicManager.playPrevious ", pVolume, pNoFade)
    local index = lMM.trackPlayed - 1
    if (index < 1) then index = #lMM.trackList end
    lMM.play(index, pVolume, pNoFade)
  end -- musicManager.playPrevious

  -- Charge les musiques par défaut liés au jeu
  -- @param pFolder le chemin du dossier contenant les musiques. Si absent, la valeur FOLDER_MUSICS sera utilisée.
  -- @param pExt (OPTIONNEL) extension des fichier à prendre en compte. Si absent,".mp3" sera utilisé
  -- @param pPrefix (OPTIONNEL) préfixe des fichier à prendre en compte. Si absent,"mmLevel" sera utilisé
  function lMM.loadDefaultMusics(pFolder, pExt, pPrefix)
    -- debugFunctionEnter("loadDefaultMusics ", pFolder, pExt, pPrefix)
    if (pFolder == nil) then pFolder  = FOLDER_MUSICS end
    if (pExt == nil or pExt == {}) then pExt = {".mp3"} end
    if (pPrefix == nil) then pPrefix  = "mmLevel" end

    -- liste tous les fichiers du répertoire
    local files = listFolderContent(pFolder, pExt, false, true, pPrefix)
    -- et les ajoute à la playlist
    local key, fileName
    for key, fileName in pairs(files) do
      lMM.add(fileName, "stream")
    end
  end

  -- initialisation par défaut
  lMM.initialize()
  return lMM
end -- createMusicManager