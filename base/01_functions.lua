-- Regroupe des fonctions utilitaires utilisées par le framework
-- ****************************************

-- ****************************************
-- GLOBAL
-- ****************************************

--- Remplace l'instruction switch
--[[
  USAGE:
  switch(case) {
    [1] = function () print"number 1!" end,
    [2] = math.sin,
    [false] = function (a) return function (b) return (a or b) and not (a and b) end end,
    Default = function (x) print"Look, Mom, I can differentiate types!" end, -- ["Default"] ;)
    [Default] = print,
    [Nil] = function () print"I must've left it in my other jeans." end,
  }
]]
Default, Nil = {}, function () end --- for uniqueness
function switch (pIndex)
  return setmetatable({pIndex}, {
    __call = function (t, cases)
      local item = #t == 0 and Nil or t[1]
      return (cases[item] or cases[Default] or Nil)(item)
    end
  })
end -- switch

--- Itérateur pour une table FINIE
--[[
USAGE 1:
  t = {10, 20, 30}
  iter = iterator(t)    -- creates the iterator
  while true do
    local element = iter()   -- calls the iterator
    if element == nil then break end
    print(element)
  end
USAGE 2:
  t = {10, 20, 30}
  for element in iterator(t) do
    print(element)
  end
]]
function iterator (pTable)
  local i = 0
  local n = table.getn(pTable)
  return function ()
    i = i + 1
    if i <= n then return pTable[i] end
  end
end -- iterator

-- ****************************************
-- CHAINES
-- ****************************************

--- fonction interne
function _titleCase (first, rest)
   return first:upper()..rest:lower()
end
--- met la 1ere lettre des mots d'une chaîne en majuscule
-- @param pString chaîne de caractères à modifier.
-- @return chaîne modifiée
function string.titleCase (pString)
  if (pString == nil or #pString < 1) then return pString end
  return string.gsub(pString, "(%a)([%w_']*)", _titleCase)
end

--- supprime les espaces dans une chaîne
-- @param pString chaîne de caractères à modifier.
-- @return chaîne modifiée
function string.trim (pString)
  if (pString == nil or #pString < 1) then return pString end
  return pString:match"^%s*(.*)":match"(.-)%s*$"
end

--- supprime les caractères invalide d'une chaîne
-- @param pString chaîne de caractères à modifier.
-- @return chaîne modifiée
function string.sanitize (pString)
  if (pString == nil or #pString < 1) then return pString end
  -- supprime tout caractère qui n'est pas alphanumérique
  local result = string.gsub(pString, "[^%w]", "")
  return result
end

--- découpe une chaîne en fonction d'un séparateur
-- @param pString chaîne de caractères à modifier.
-- @param pDelimiter séparateur.Si absent, la valeur "," sera utilisée
-- @return table contenant les éléments séparés
--[[
  usage
  t = split("a,b,c,d,e,f,g",",")
  for i,j in pairs(t) do
    print(i,j)
  end
]]
function string.split (pString, pDelimiter)
  local fields = {}
  if (pDelimiter == nil) then pDelimiter = "," end
  local pattern = string.format("([^%s]+)", pDelimiter)
  string.gsub(pString, pattern, function(c) fields[#fields + 1] = c end)
  return fields
end

--- récupère les paires variable = valeur contenues dans une table
-- @param pTable table à analyser.
-- @param pDelimiter séparateur entre chaque variable et sa valeur. Si absent, la valeur "=" sera utilisée.
-- @return table contenant les éléments sous la forme {variable=valeur,variable=valeur...}
function table.parseValues (pTable, pDelimiter)
  if (pDelimiter == nil) then pDelimiter = "=" end
  local i, var, value
  local result = {}
  -- boucle sur les entrées de la table
  for i = 1, #pTable do
    -- sépare les paires variable = valeur
    local temp = pTable[i]:split("=")
    var = temp[1]
    value = temp[2]
    -- supprime les espaces
    var = var:trim()
    value = value:trim()
    result[var] = value
  end -- for i
  return result
end

-- ****************************************
-- MATHS
-- ****************************************

--- Retourne l'angle en radiant de la droite passant par 2 points (utile pour les déplacements en X et Y)
-- Ajoutée au module math
-- usage:
-- pSprite.angle = math.angle(pSprite.x, pSprite.y, pSprite2.x, pSprite2.y)
-- vX = speed * math.cos(pSprite.angle)
-- vY = speed * math.sin(pSprite.angle)
-- @param pX1 position X du point 1
-- @param pY1 position Y du point 1
-- @param pX2 position X du point 2
-- @param pY2 position Y du point 2
-- @return angle en radiant
function math.angle (pX1, pY1, pX2, pY2)
  return math.atan2(pY2 - pY1, pX2 - pX1)
end -- math.angle

-- Retourne la distance entre 2 points.
-- Ajoutée au module math
-- @param pX1 position X du point 1
-- @param pY1 position Y du point 1
-- @param pX2 position X du point 2
-- @param pY2 position Y du point 2
-- @return distance en pixel
function math.dist (x1, y1, x2, y2)
  return ((x2 - x1)^ 2  + (y2 - y1) ^ 2) ^ 0.5
end

-- ****************************************
-- TABLES
-- ****************************************

--- Mélange le contenu d'une table
-- @param pTable table à mélanger
-- @return la table mélangée
-- @see https://forums.coronalabs.com/topic/195-random-table-shuffle/
function table.shuffle (pTable)
  local count = #pTable
  for i = 1, (count * 20) do
    local index0 = math.random(1, count)
    local index1 = math.random(1, count)
    local temp = pTable [index0]
    pTable[index0] = pTable [index1]
    pTable[index1] = temp
  end
  return pTable
end -- table.shuffle

--- merge le contenu de 2 tables
-- @param pFirstTable 1ere table. Son contenu sera modifié
-- @param secondTable 2e table
-- @return la 1ere table avec en plus le contenu de la seconde
function table.merge (pFirstTable, pSecondTable)
  for k,v in pairs(pSecondTable) do
    pFirstTable[k] = v
  end
  return pFirstTable
end -- table.merge

--- Met à nil tous les éléments d'une table NON recursivement
-- @param pTable table à effacer.
function table.empty (pTable)
  if (pTable == nil or pTable == {}) then return end
  local index, value
  for index, value in pairs(pTable) do
    value = nil
  end
end -- table.empty

-- ****************************************
-- DEBUG et LOG
-- ****************************************

--- Affiche le contenu d'une variable
-- @param pVar variable à afficher
function dump (pVar)
  if (pVar == nil) then
    print "NIL"
  elseif (type(pVar) == "table") then
    for k, v in pairs(pVar) do
      print(tostring(k).." = "..tostring(v))
    end
  else
    print(pVar)
  end
end -- dump

--- Retourne TRUE si une une variable est égale à une valeur donnée, FALSE sinon et affiche un message optionnel
-- @param pVar variable à tester
-- @param pValue valeur à comparer
-- @param pMessage (OPTIONNEL) texte à afficher quand l'assertion est VRAIE
-- @return true si l'assertion est vraie, false sinon
function assertEqual (pVar, pValue, pMessage)
  if (pVar == pValue) then
    if (pMessage ~= nil) then
      print ("assertEqual: ", pMessage, " Value=", tostring(pValue))
    end
    return true
  else
    return false
  end
end -- assertEqual

--- Quitte le jeu si une une variable est égale à une valeur donnée, et affiche un message optionnel
-- @param pVar variable à tester
-- @param pValue valeur à comparer
-- @param pMessage (OPTIONNEL) texte à afficher quand l'assertion est VRAIE
function assertEqualQuit (pVar, pValue, pMessage)
  if (pVar == pValue) then
    if (pMessage ~= nil) then
       print ("assertEqualQuit: ", pMessage, " Value=", tostring(pValue))
    end
    print ("ARRET SUR VALIDATION D'UNE ASSERTION")
    love.event.push('quit')
  end
end -- assertEqualQuit

--- Logue (Affiche) un message d'erreur
-- @param pMessage texte à loguer
-- @param mustQuit (OPTIONNEL) si vrai, termine le jeu après avoir afficher le message
function logError (pMessage, pMustQuit)
  print (pMessage)
  if (pMustQuit) then love.window.close() end
end -- logError

--- Logue (Affiche) des contenus pour du debug
-- @param Paramètres dynamiques
function debugMessage (...)
  if (DEBUG_MODE >= 1) then
    print(os.date().."# INFO #", ...)
  end
end -- debugMessage

--- Logue (Affiche) des contenus pour du debug lors de l'entrée dans une fonction SANS les appels qui peuvent remplir le log
-- @param Paramètres dynamiques
function debugFunctionEnter (...)
  if (DEBUG_MODE >= 4) then
    print(os.date().."# ENTREE FONCTION #", ...)
  end
end -- debugFunctionEnter

--- Logue (Affiche) des contenus pour du debug lors de l'entrée dans une fonction AVEC les appels qui peuvent remplir le log
-- @param Paramètres dynamiques
function debugFunctionEnterNL (...)
  if (DEBUG_MODE >= 5) then
    print(os.date().."# ENTREE FONCTION #", ...)
  end
end -- debugFunctionEnterNL

---Affiche un point rouge au coordonnées données
-- @param pX position X du point
-- @param pY position Y du point
function debugPoint (pX, pY)
  love.graphics.setColor(255, 0, 0, 255)
  love.graphics.rectangle("fill", pX, pY, 10, 10)
  love.graphics.setColor(255, 255, 255, 255)
end -- debugPoint

--- Logue (Affiche) des contenus d'information
-- @param Paramètres dynamiques
function logMessage (...)
  print(os.date().."# LOG #", ...)
end -- logMessage

-- ****************************************
-- IMAGES
-- ****************************************

--- Créer un objet image (love) en vérifiant si le fichier existe
-- @param filename Nom du fichier
-- @return l'objet image ou nil si le fichier n'existe pas
function newImageIfExists (filename)
  if (filename ~= nil and filename ~= "" and love.filesystem.isFile(filename)) then
    return love.graphics.newImage(filename)
  else
    return nil
  end
end -- newImageIfExists

--- Ajoute les images contenues dans un dossier à une liste
-- @param pFolder le chemin du dossier contenant les images
-- @param pExtList (OPTIONNEL) tableau contenant les extensions des fichier à prendre en compte. Si absent, {".png"} sera utilisé
-- @return une table contenant les images associées aux fichiers images trouvés dans le dossier
function addImages (pFolder, pExtList)
  -- debugFunctionEnter("addImages ", pFolder, pExtList)
  if (pExtList == nil or pExtList == {}) then pExtList  = {".png"} end
  local imageList = {}
  local imageTypeList = {}
  local allFiles = listFolderContent(pFolder, pExtList, false, true)
  -- ajoute les images à la table des images retournées
  local i
  for i = 1, #allFiles do
    local index = #imageList + 1
    imageList[index] = resourceManager.getImages(allFiles[i])
  end

  return imageList
end -- addImages

--- Ajoute les resources contenues dans un dossier à une liste
-- @param pFolder le chemin du dossier contenant les ressources
-- @param pExtList (OPTIONNEL) tableau contenant les extensions des fichier à prendre en compte.
-- @return une table contenant les ressources associées aux fichiers trouvés dans le dossier
function addResources (pFolder, pExtList)
  -- debugFunctionEnter("addResources ", pFolder, pExtList)
  local resourceList = {}
  local allFiles = listFolderContent(pFolder, pExtList, false, true)
  -- ajoute les images à la table des images retournées
  local i
  for i = 1, #allFiles do
    local index = #resourceList + 1
    resourceList[index] = resourceManager.get(allFiles[i]) -- type de ressource déterminé automatiquement par l'extension de fichier
  end

  return resourceList
end -- addResources

-- ****************************************
-- AFFICHAGE
-- ****************************************

-- ****************************************
-- COLLISIONS
-- ****************************************

--- Vérifie si 2 sprites entre en collision (collision rectangulaire de la taille des sprites)
-- @param pSprite 1er sprite à vérifier.
-- @param pSprite 2e sprite à vérifier.
-- @param pStatusToIgnore Status de sprite devant être ignoré.
-- @return true ou false
function boxCollide (pSprite1, pSprite2, pStatusToIgnore)
  ---- debugFunctionEnterNL("boxCollide ", pSprite1, pSprite2, pStatusToIgnore) -- ATTENTION  cet appel peut remplir le log
  if (pStatusToIgnore == nil) then
    pStatusToIgnore = "ZZXXZZ" -- ainsi, la condition sera toujours ignorée si le status à tester est nil
  end
  if (
    (pSprite1 == pSprite2)
    or (pSprite1.status == pStatusToIgnore)
    or (pSprite2.status == pStatusToIgnore)
  )
  then
    return false
  end
  if (pSprite1.x < pSprite2.x + pSprite2.width and
    pSprite2.x < pSprite1.x + pSprite1.width and
    pSprite1.y < pSprite2.y + pSprite2.height and
    pSprite2.y < pSprite1.y + pSprite1.height
    ) then
    return true
  end
  return false
end -- boxCollide

-- ****************************************
-- FICHIERS
-- ****************************************

--- retourne le path d'un nom de fichier complet
-- @param @fileName nom du fichier à analyser
-- @return le path du fichier
function getFilePath (fileName)
    return fileName:match("(.*[/\\])")
end

--- retourne le nom de fichier d'un nom de fichier complet
-- @param @fileName nom du fichier à analyser
-- @return le nom du fichier
function getFileName (fileName)
  return fileName:match("^.+/(.+)$")
end

--- retourne l'extension d'un nom de fichier complet
-- @param @fileName nom du fichier à analyser
-- @return l'extension du fichier
function getFileExtension (fileName)
  local result = fileName:match("^.+(%..+)$")
  result = result:sub(2)
  return result
end

-- lit un fichier texte et retourne son contenu
-- @param @fileName nom du fichier à lire
-- @return le contenu du fichier sous forme d'une table
function readTextFile (pFileName)
  local line
  local fileContent = {}
  for line in love.filesystem.lines(pFileName) do
    fileContent[#fileContent + 1] = line
  end
  return fileContent
end

--- Ajoute les fichiers contenus dans un dossier à une liste
-- @param pFolder le chemin du dossier contenant les fichiers
-- @param pExtList (OPTIONNEL) tableau contenant les extensions des fichier à prendre en compte. Si absent, tous les fichiers seront listés.
-- @param pScanSubFolders (OPTIONNEL) true pour faire une recherche dans les sous-dossiers. Si absent, la valeur false sera utilisée.
-- @param pSortFiles (OPTIONNEL) true pour trier le résultat par ordre alphabétique croissant. Si absent, la valeur true sera utilisée.
-- @param pPrefix (OPTIONNEL) préfixe des fichier à prendre en compte. Si absent, tous les fichiers seront listés.
-- @return une table contenant le nom complet des fichiers trouvés dans le dossier
function listFolderContent (pFolder, pExtList, pScanSubFolders, pSortFiles, pPrefix)
  -- debugFunctionEnter("ListFolderContent ", pFolder, pExt, pScanSubFolders, pSortFiles, pPrefix)
  if (pExtList == nil) then pExtList = {} end
  if (pScanSubFolders == nil) then pScanSubFolders = false end
  if (pSortFiles == nil) then pSortFiles = true end
  if (pPrefix == nil) then pPrefix = "" end

  local key, value, fileNameLen, fileExt, filePrefix, pExtLen, pPrefixLen, pExt, i
  local allFiles = {}
  pPrefix = string.lower(pPrefix)
  pPrefixLen = string.len(pPrefix)

  -- liste tous les fichiers du répertoire
  local files = love.filesystem.getDirectoryItems(pFolder)

  for key, value in pairs(files) do
    local fileName = pFolder.."/"..value
    if (love.filesystem.isFile(fileName)) then
      -- si l'élément courant est un fichier
      -- compare l'extension du fichier et celles passées en paramètre
      for i = 1, #pExtList do
        pExt = string.lower(pExtList[i])
        pExtLen = string.len(pExt)
        fileNameLen = string.len(fileName)
        fileExt = string.sub(string.lower(fileName), -pExtLen)
        -- les extensions de fichier correspondent-elles ?
        if (pExt == nil or pExt == "" or fileExt == pExt) then
          --oui, on compare le préfixe du fichier et celui recherché
          filePrefix = string.sub(string.lower(value), 1, pPrefixLen)
          if (pPrefix == "" or filePrefix == pPrefix) then
            allFiles[#allFiles + 1] = fileName
          end
        end
      end -- for
    elseif (love.filesystem.isDirectory(fileName)) then
      -- si l'élément courant est un dossier
      -- on liste son contenu avec les même paramètres
      local folderContent = listFolderContent(fileName, pExtList , pScanSubFolders, pSortFiles, pPrefix)
      -- et on ajoute les fichiers qu'il contient à la liste
      local i
      for i = 1, #folderContent do
        allFiles[#allFiles + 1] = folderContent[i]
      end -- for
    end -- if (love.filesystem.isFile(fileName)) then
  end -- for
  if (pSortFiles) then
    -- trie les nom de fichiers (pour ordonner les images de l'animation)
    table.sort(allFiles)
  end
  return allFiles
end -- ListFolderContent

--- Charge automatiquement les fichier lua contenu dans un répertoire donné.
-- @param pFolder Nom du dossier à parcourir.
-- @param pScanSubFolders (OPTIONNEL) true pour faire une recherche dans les sous-dossiers. Si absent, la valeur false sera utilisée.
function autoLoadRequire (pFolder, pScanSubFolders)
  if (pScanSubFolders == nil) then pScanSubFolders = false end
  local ext = ".lua"

  local luaFiles = listFolderContent(pFolder, {ext}, true, false)
  local key, file, fullFileNameNoExt, i, fileName
  for i = 1, #luaFiles do
    -- on fait un require uniquement des fichier dont le nom ne contient pas la constante NO_AUTOLOAD_STRING
    fileName = luaFiles[i]
    if (fileName:find(NO_AUTOLOAD_STRING) == nil) then
      fullFileNameNoExt = fileName:gsub(ext, "")
      require(fullFileNameNoExt)
    end
  end -- for
end


-- ****************************************
-- DIVERS
-- ****************************************
--- retourne le contenu du dernier suffixe d'une chaîne utilisant un construction composée (comme les types de sprites)
-- @param pType Chaîne à analyser.
-- @return contenu du suffixe (sans le séparateur) ou "" si non trouvé.
function getLastSuffix (pType)
  local pos = pType:find(SEP_LAST)
  local sepLastLen = #SEP_LAST
  local result = ""
  if (pos ~= nil) then
    result = pType:sub(pos + sepLastLen)
  end
  return result
end

--- Détermination des points entourant une série de point d'une liste de points.
-- NOTE: Permet de calculer la forme englobante. Utile pour les collisions de forme complexes
-- @param pVertices Liste des sommets (paires de coordonnées sous la forme {x,y}) du polygone
-- @return Liste des sommets triés pour créer un polygone convexe
function createConvexHull (pVertices)
  -- Andrew's monotone chain convex hull algorithm
  -- https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
  -- Direct port from Javascript version
  local p = #pVertices

  local cross = function(p, q, r)
    return (q[2] - p[2]) * (r[1] - q[1]) - (q[1] - p[1]) * (r[2] - q[2])
  end

  table.sort(pVertices, function(a, b)
    return a[1] == b[1] and a[2] > b[2] or a[1] > b[1]
  end)

  local lower = {}
  for i = 1, p do
    while (#lower >= 2 and cross(lower[#lower - 1], lower[#lower], pVertices[i]) <= 0) do
      table.remove(lower, #lower)
    end

    table.insert(lower, pVertices[i])
  end

  local upper = {}
  for i = p, 1, -1 do
    while (#upper >= 2 and cross(upper[#upper - 1], upper[#upper], pVertices[i]) <= 0) do
      table.remove(upper, #upper)
    end

    table.insert(upper, pVertices[i])
  end

  table.remove(upper, #upper)
  table.remove(lower, #lower)
  for _, point in ipairs(lower) do
    table.insert(upper, point)
  end

  return upper
end