-- Entité représentant un personnage joueur du jeu
-- Utilise une forme d'héritage simple: character qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
SPRITE_TYPE_CHARACTER = SPRITE_TYPE..SPRITE_EXT_CHARACTER

--- Crée un pseudo objet de type character
-- Note: certaines valeurs sont divisées par l'échelle de l'écran
-- @param pType type de personnage à créer.
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur 0 sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur 0 sera utilisée.
-- @param pWidth (OPTIONNEL) largeur en pixel. Si absent la valeur map.tilewidth sera utilisée.
-- @param pHeight (OPTIONNEL) hauteur en pixel. Si absent la valeur map.tileheight sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la va²leur "" sera utilisée.
-- @param pVelocityJump (OPTIONNEL) vélocité pour un saut. Si absent la valeur 0 sera utilisée.
-- @param pSpeed (OPTIONNEL) vitesse initiale. Si absent la valeur 0 sera utilisée.
-- @param pFriction (OPTIONNEL) freinage du à l'environnement. Si absent la valeur settings.friction sera utilisée.
-- @param pGravity (OPTIONNEL) valeur de la gravité. Si absent la valeur settings.gravity sera utilisée.
-- @param pVelocityMax (OPTIONNEL) vélocité maximale. Si absent la valeur 0 sera utilisée. Si la valeur est 0, l'objet ne pourra pas se déplacer. Si absent la valeur 0 sera utilisée.
-- @param pWeight (OPTIONNEL) poids (influence la gravité et la résistance de l'environnement). Si la valeur est nulle, l'objet ne sera pas soumis à la gravité et au freinage. Si absent la valeur 1 sera utilisée.
-- @param pLifeImage (OPTIONNEL) chemin pour l'image représentant une vie. Mettre à "" pour ne pas utiliser d'image pour l'affichage des vies. Si absent la valeur FOLDER_PLAYER_IMAGES.."/playerLife.png" sera utilisée.
-- @param pLevel (OPTIONNEL) niveau. Mettre à -1 pour ne pas afficher la valeur dans le HUD. Si absent la valeur 1 sera utilisée.
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent, la valeur 1 sera utilisée.
-- @param pScore (OPTIONNEL) score initial. Mettre à -1 pour ne pas afficher la valeur dans le HUD. Si absent la valeur 0 sera utilisée.
-- @param pUseFriction (OPTIONNEL) true pour utiliser les vélocités pour le déplacement de l'objet (mouvement automatique). Si absent la valeur true sera utilisée.
-- @param pMoveWithMouse (OPTIONNEL) true pour utiliser la souris pour le déplacement de l'objet. Si absent la valeur true sera utilisée.
-- @param pMoveWithKeys (OPTIONNEL) true pour utiliser les touches du clavier pour le déplacement de l'objet. Si absent la valeur true sera utilisée.
-- @param pColor (OPTIONNEL) couleur par défaut (utile si aucune image n'est définie pour l'afficher). Si absent, la valeur {128, 128, 128, 255}  sera utilisée.
-- @param pvX (OPTIONNEL) vélocité en X. Si absent la valeur 0 sera utilisée.
-- @param  (OPTIONNEL) vélocité en Y. Si absent la valeur 0 sera utilisée.
-- @param pCanCollideWithTile (OPTIONNEL) true pour que le sprite puisse collisioner avec les tuiles de la carte. Si absent, la valeur true sera utilisée.
-- @param pCanJump (OPTIONNEL) true pour que le sprite puisse sauter. Si absent, la valeur true sera utilisée.
-- @param pUseVelocity (OPTIONNEL) true pour utiliser les vélocités pour le déplacement de l'objet (si oui, le mouvement aura de l'inertie, sinon il sera instantané). Si absent, la valeur true sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur 1 sera utilisée.
-- @param pMustRespawnIfDestroyed (OPTIONNEL) true pour re-spawner une fois si détruit. Si absent, la valeur true sera utilisée.
-- @return un pseudo objet character, index du character dans entities.characters
function createCharacter (pType, pX, pY, pWidth, pHeight, pImage, pVelocityJump, pSpeed, pFriction, pGravity, pVelocityMax, pWeight, pLifeImage, pLevel, pLife, pScore, pUseFriction, pMoveWithMouse, pMoveWithKeys, pColor, pvX, pvY, pCanCollideWithTile, pCanJump, pUseVelocity, pEnergy)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createCharacter ", pType, pX, pY, pWidth, pHeight, pImage, pVelocityJump, pSpeed, pFriction, pGravity, pVelocityMax, pWeight, pLifeImage, pLevel, pLife, pScore, pUseFriction, pMoveWithMouse, pMoveWithKeys, pColor, pvX, pvY, pCanCollideWithTile, pCanJump, pUseVelocity, pEnergy)

  assertEqualQuit(viewport, nil, "character.initialize:viewport", true)
  assertEqualQuit(settings.playerKeys, nil, "character.initialize:settings.playerKeys", true)

  if (pX == nil) then pX = 0 end
  if (pY == nil) then pY = 0 end
  if (pImage == nil) then pImage = "" end
  if (pVelocityJump == nil) then pVelocityJump = 0 end
  if (pSpeed == nil) then pSpeed = 0 end
  if (pFriction == nil) then pFriction = settings.friction end
  if (pGravity == nil) then pGravity = settings.gravity end
  if (pVelocityMax == nil) then pVelocityMax = 0 end
  if (pWeight == nil) then pWeight = 1 end
  if (pLifeImage == nil) then pLifeImage = FOLDER_PLAYER_IMAGES.."/playerLife.png" end
  if (pLevel == nil) then pLevel = 1 end
  if (pLife == nil) then pLife = 3 end
  if (pScore == nil) then pScore = 0 end
  if (pUseFriction == nil) then pUseFriction = true end
  if (pMoveWithKeys == nil) then pMoveWithKeys = true end
  if (pMoveWithMouse == nil) then pMoveWithMouse = true end
  if (pColor == nil) then pColor = {128, 128, 128, 255} end
  if (pvX == nil) then pvX = 0 end
  if (pvY == nil) then pvY = 0 end
  if (pCanCollideWithTile == nil) then pCanCollideWithTile = true end
  if (pCanJump == nil) then pCanJump = true end
  if (pUseVelocity == nil) then pUseVelocity = true end
  if (pEnergy == nil) then pEnergy = 1 end
  if (pMustRespawnIfDestroyed == nil) then pMustRespawnIfDestroyed = true end

  -- Note: les valeurs suivantes sont divisées par l'échelle de l'écran
  pSpeed = math.floor(pSpeed / DISPLAY_SCALE_X)
  pVelocityMax = math.floor(pVelocityMax / DISPLAY_SCALE_X)
  pVelocityJump = math.floor(pVelocityJump / DISPLAY_SCALE_Y)
  pGravity = math.floor(pGravity / DISPLAY_SCALE_Y)

  -- rappel: createSprite(pImage, pX,pY,pvX,pvY,pType,pWeight,pLife,pEnergy,pPoints,pCanCollideWithTile,pVelocityMax,pVelocityJump,pGravity,pCanMakeAction,pStatus,pMustDestroyedAtBorder,pTimeToLive,pMustDestroyedOnCollision,pSpeed,pMustRespawnIfDestroyed)
  local lCharacter = createSprite(pImage, pX, pY, pvX, pvY, pType, pWeight, pLife, pEnergy, 0, pCanCollideWithTile, pVelocityMax, pVelocityJump, pGravity, true, SPRITE_STATUS_NORMAL, false, 0, false, pSpeed, pMustRespawnIfDestroyed)

  lCharacter.destroyDelay = 3
  lCharacter.canCollideWithTile = pCanCollideWithTile
  lCharacter.canJump = pCanJump
  lCharacter.color = pColor
  lCharacter.currentAnimation = ANIM_NAME_NONE
  lCharacter.mustMoveWithCamera = false

  -- attributs spécifiques au player
  -- ******************************
  -- Identifiant du joueur (en mode multi-joueur)
  lCharacter.id = 1 -- modifié lors de l'ajout à la liste des joueurs
  -- utilise les touches du clavier pour le déplacement de l'objet
  lCharacter.moveWithMouse = pMoveWithMouse
  -- utilise la souris pour le déplacement de l'objet
  lCharacter.moveWithKeys = pMoveWithKeys
  -- liste des touches de clavier devant être relâchées avant une action
  lCharacter.keysToRelease = {}
  -- les touches utilisées pour les déplacements du joueur (propres à chaque joueur)
  lCharacter.keys = settings.playerKeys -- par défaut on utilise les touches de clavier définies dans les réglages du jeu
  -- niveau
  lCharacter.level = pLevel
  -- chemin pour l'image représentant une vie
  lCharacter.lifeImage = pLifeImage
  -- freinage du à l'environnement'
  lCharacter.friction = math.floor(pFriction / DISPLAY_SCALE_X)
  -- utilise ou pas la friction pour le déplacement de l'objet (mouvement automatique)
  -- NOTE. si true le mouvement utilisera l'inertie et le freinage, sinon les déplacements seront direct (déplacements standards)
  lCharacter.useFriction = pUseFriction
  -- utilise ou pas les vélocités pour le déplacement de l'objet (si oui, le mouvement aura de l'inertie, sinon il sera instantané)
  lCharacter.useVelocity = pUseVelocity
  -- score
  lCharacter.score = pScore
  -- le joueur a t'il gagné la partie ?
  lCharacter.hasWon = false
  -- nom du joueur
  lCharacter.nickName = ""

  -- Touches devant être relachées avant une action.
  -- keysToRelease[settings.playerKeys.moveDown] = false
  -- keysToRelease[settings.playerKeys.moveDownAlt] = false
  --- Initialise le player
  function lCharacter.initialize ()
    debugFunctionEnter("character.initialize")

    -- définit les hotSpots pour les collisions
    lCharacter.createCollisionBox(COLLISION_MODEL_BOX6)

    lCharacter.addSound("WinEnergy")
    lCharacter.addSound("Fall")
    lCharacter.addSound("PickUp")
    lCharacter.addSound("Jump")
    lCharacter.addSound("Standing")
    lCharacter.addSound("Climb")
		lCharacter.updateInitialValues() -- important
	end -- character.initialize

  -- mémorise certains des paramètres initiaux du sprite
  -- cela permet de retrouver ces valeurs car elle peuvent être modifiées pendant le jeu
  function lCharacter.updateInitialValues ()
    debugFunctionEnter("character.updateInitialValues")
    local spriteInitialValues = lCharacter.initialValues
    local lInitialValues = {
      level = lCharacter.level,
      lifeImage = lCharacter.lifeImage,
      friction = lCharacter.friction,
      useFriction = lCharacter.useFriction,
      useVelocity = lCharacter.useVelocity,
      keys = lCharacter.keys
    }
    lCharacter.initialValues = table.merge (spriteInitialValues, lInitialValues)
  end -- character.updateInitialValues

  -- Accesseurs pour les attributs de cet objet
  -- ******************************
  function lCharacter.get_keyToRelease (pIndex)
    return lCharacter.keysToRelease[pIndex]
  end
  function lCharacter.set_keyToRelease (pIndex, pValue)
    lCharacter.keysToRelease[pIndex] = pValue
  end

  -- Override des fonctions du sprite
  -- ******************************

  --- affiche le joueur
  lCharacter.spriteDraw = lCharacter.draw -- mémorise la fonction du parent (sprite) afin de l'appeler si nécessaire
  function lCharacter.draw ()
    ---- debugFunctionEnterNL("character.draw") -- ATTENTION  cet appel peut remplir le log

     -- affiche des effets visuels spécifiques
     -- TODO : améliorer les effets visuels en fonction de l'état du joueur
    local effectColor, mode
    local effectSize = 1
    local offsetX = 10
    local offsetY = 10
    local halfW = lCharacter.width / 2
    local halfH = lCharacter.height / 2
    local status = lCharacter.status
    local opacity = 60 / lCharacter.initialValues.energy * lCharacter.energy
    local drawMode = 1 -- 0, pas d'affichage de l'effet, 1 = affichage du sprite dessous l'effet, 2 = affichage du sprite dessus l'effet

    -- on n'affiche l'effet que si l'énergie est supérieure à 1
    if (lCharacter.energy > 1) then drawMode = 1 else drawMode = 0 end

    if (status == SPRITE_STATUS_INVISIBLE) then
      -- blanc quasi transparent, plein
      mode = "fill"
      effectColor = {128, 128, 128, 30}
    elseif (status == SPRITE_STATUS_INVINCIBLE) then
      -- bleu clair, vide
      mode = "line"
      effectColor = {0, 128, 255, 200}
    elseif (status == SPRITE_STATUS_DEADLY) then
      -- rouge vif, vide
      mode = "line"
      effectColor = {255, 0, 0, 200}
    elseif (status == SPRITE_STATUS_FREEZE) then
      -- bleu clair semi transparent, vide
      mode = "line"
      effectColor = {194, 254, 253, 200}
    else
      -- état normal, on affiche juste un bouclier représentant l'énergie du joueur
      -- cyan quasi transparent, plein
      mode = "fill"
      effectColor = {128, 255, 255, opacity}
    end

    if (drawMode <= 1) then lCharacter.spriteDraw() end
    if (drawMode > 0) then
    love.graphics.setColor(effectColor)
    love.graphics.setLineWidth(effectSize)
      love.graphics.ellipse (mode, lCharacter.x + halfW, lCharacter.y + halfH, halfW + offsetX, halfH + offsetY)
    love.graphics.setLineWidth(1)
    end
    if (drawMode == 2) then lCharacter.spriteDraw() end

    love.graphics.setColor (255, 255, 255, 255)
  end -- character.draw

  --- Détruit le joueur
  -- @param pShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lCharacter.spriteDestroy = lCharacter.destroy -- mémorise la fonction du parent (sprite) afin de l'appeler si nécessaire
  function lCharacter.destroy (pShowFX, pPlaySound)
    debugFunctionEnter("character.destroy ", pShowFX, pPlaySound)
    -- ?? if (lCharacter.status ~= SPRITE_STATUS_NORMAL) then return end

    if (pShowFX == nil) then pShowFX = lCharacter.mustDestroyedWithFX end
    if (pPlaySound == nil) then pPlaySound = lCharacter.mustDestroyedWithSound end

    if (pShowFX) then createEffect(SPRITE_TYPE_EFFECT_EXPLODE_PLAYER) end
    if (pPlaySound) then lCharacter.playSound("Destroy") end
    -- la destruction effective se fait dans la classe parent
    lCharacter.spriteDestroy()
  end -- character.destroy

  --- Actualise le joueur
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lCharacter.spriteUpdate = lCharacter.update
  function lCharacter.update (pDt)
    ---- debugFunctionEnterNL("character.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lCharacter.spriteUpdate(pDt)

    if (lCharacter.status == SPRITE_STATUS_DESTROYING or lCharacter.status == SPRITE_STATUS_DESTROYED) then return end

    -- par défaut, adapte la direction du sprite en fonction de son état
    if (lCharacter.currentAnimation.flipedToRight) then
      lCharacter.direction = SPRITE_MOVE_LEFT
    else
      lCharacter.direction = SPRITE_MOVE_RIGHT
    end
    if (lCharacter.isJumping) then
      lCharacter.direction = SPRITE_MOVE_UP
    elseif (lCharacter.isFalling) then
      lCharacter.direction = SPRITE_MOVE_DOWN
    end

    -- déplacement avec le clavier
    if (appState.currentScreen == SCREEN_PLAY) then
      if (lCharacter.moveWithKeys and lCharacter.checkIfActive()) then
        -- le player peut il collisioner avec les tuiles ?
        if (not lCharacter.checkIfCanCollideWithTiles()) then
          -- map sans tuile ou sans collision avec les tuiles (type shooter), les déplacements sont "normaux", sans contraintes spécifiques
          -- ******************************
          if (lCharacter.useVelocity) then
              -- déplacements avec utilisation des vélocités
            if (love.keyboard.isDown(lCharacter.keys["moveRight"]) or love.keyboard.isDown(lCharacter.keys["moveRightAlt"])) then
              lCharacter.vX = (lCharacter.vX + lCharacter.speed * pDt)
              lCharacter.direction = SPRITE_MOVE_RIGHT
            end
            if (love.keyboard.isDown(lCharacter.keys["moveLeft"]) or love.keyboard.isDown(lCharacter.keys["moveLeftAlt"])) then
              lCharacter.vX = (lCharacter.vX - lCharacter.speed * pDt)
              lCharacter.direction = SPRITE_MOVE_LEFT
            end
            if (love.keyboard.isDown(lCharacter.keys["moveUp"]) or love.keyboard.isDown(lCharacter.keys["moveUpAlt"])) then
              lCharacter.vY = (lCharacter.vY - lCharacter.speed * pDt)
              lCharacter.direction = SPRITE_MOVE_UP
            end
            if (love.keyboard.isDown(lCharacter.keys["moveDown"]) or love.keyboard.isDown(lCharacter.keys["moveDownAlt"])) then
              lCharacter.vY = (lCharacter.vY + lCharacter.speed * pDt)
              lCharacter.direction = SPRITE_MOVE_DOWN
            end

            if (lCharacter.velocityMax >= 0) then
              -- rappel: on ne tient pas compte de la vélocité max si elle est inférieure à 0
              -- limite la vélocité en X
              if (lCharacter.vX < -lCharacter.velocityMax) then
                lCharacter.vX = -lCharacter.velocityMax
              end
              if (lCharacter.vX > lCharacter.velocityMax) then
                lCharacter.vX = lCharacter.velocityMax
              end
              -- limite la vélocité en Y
              if (lCharacter.vY < -lCharacter.velocityMax) then
                lCharacter.vY = -lCharacter.velocityMax
              end
              if (lCharacter.vY > lCharacter.velocityMax) then
                lCharacter.vY = lCharacter.velocityMax
              end
            end
          else
            local xMin, yMin, xMax, yMax = lCharacter.getScreenLimits()
            -- déplacements sans utilisation des vélocités
            if (love.keyboard.isDown(lCharacter.keys["moveRight"]) or love.keyboard.isDown(lCharacter.keys["moveRightAlt"]) and lCharacter.x < xMax) then
              lCharacter.x = lCharacter.x + lCharacter.speed * pDt
              lCharacter.direction = SPRITE_MOVE_RIGHT
            elseif (love.keyboard.isDown(lCharacter.keys["moveLeft"]) or love.keyboard.isDown(lCharacter.keys["moveLeftAlt"]) and lCharacter.x > xMin) then
              lCharacter.x = lCharacter.x - lCharacter.speed * pDt
              lCharacter.direction = SPRITE_MOVE_LEFT
            end
            if (love.keyboard.isDown(lCharacter.keys["moveUp"]) or love.keyboard.isDown(lCharacter.keys["moveUpAlt"]) and lCharacter.y > yMin) then
              lCharacter.y = lCharacter.y - lCharacter.speed * pDt
              lCharacter.direction = SPRITE_MOVE_UP
            elseif (love.keyboard.isDown(lCharacter.keys["moveDown"]) or love.keyboard.isDown(lCharacter.keys["moveDownAlt"]) and lCharacter.y < yMax) then
              lCharacter.y = lCharacter.y + lCharacter.speed * pDt
              lCharacter.direction = SPRITE_MOVE_DOWN
            end
          end
        else
          -- avec tuile et collision possible, les déplacements sont en fonction des tuiles autour du joueur
          -- ******************************
          -- Id des Tuiles sous le joueur
          local tx = math.floor(lCharacter.x)
          local ty = math.floor(lCharacter.y)
          local ty2 = math.floor(lCharacter.y + map.tileheight)
          local idUnder = map.getTileAt(tx, ty)
          local idOverlap = map.getTileAt(tx, ty2)

          -- Vérifie si le joueur a fini de sauter ou s'il est sur une échelle
          if (lCharacter.isJumping and (lCharacter.collideWithTileBottom() ~= SPRITE_COLLISION_NONE or map.isLadder(idUnder))) then
            lCharacter.isJumping = false
            lCharacter.playSound("Standing")
            lCharacter.updateLastStanding(true)
            lCharacter.alignOnLineTile()
          end

          -- Touches droite et gauche du Clavier
          -- rappel: on ne tient pas compte de la vélocité max si elle est inférieure à 0
          if (love.keyboard.isDown(lCharacter.keys["moveRight"]) or love.keyboard.isDown(lCharacter.keys["moveRightAlt"])) then
            lCharacter.vX = lCharacter.vX + lCharacter.speed * pDt
            if (lCharacter.velocityMax >= 0 and lCharacter.vX > lCharacter.velocityMax) then lCharacter.vX = lCharacter.velocityMax end
            lCharacter.currentAnimation.flipedToRight = false
          end
          if (love.keyboard.isDown(lCharacter.keys["moveLeft"]) or love.keyboard.isDown(lCharacter.keys["moveLeftAlt"])) then
            lCharacter.vX = lCharacter.vX - lCharacter.speed * pDt
            if (lCharacter.velocityMax >= 0 and lCharacter.vX < -lCharacter.velocityMax) then lCharacter.vX = -lCharacter.velocityMax end
            lCharacter.currentAnimation.flipedToRight = true
          end

          -- Vérifie si le joueur est sur une échelle
          local isOnLadder = map.isLadder(idUnder) or map.isLadder(idOverlap)

          if (not map.isLadder(idOverlap) and map.isLadder(idUnder)) then
            lCharacter.updateLastStanding(true)
          end
          --if (map.isLadder(idOverlap) and map.isLadder(idUnder)) then
          --createAnimationName = ANIM_NAME_MOVE_CLIMB_IDLE
          --end

          -- Touches haut : Sauter
          if ((love.keyboard.isDown(lCharacter.keys["moveUp"]) or love.keyboard.isDown(lCharacter.keys["moveUpAlt"])) and lCharacter.isStanding and lCharacter.canJump and not map.isLadder(idOverlap)) then
            lCharacter.jump()
          end

          -- Vérifie si le joueur grimpe
          if (isOnLadder and not isJumping) then
            lCharacter.gravity = 0
            lCharacter.vY = 0
            lCharacter.canJump = false
            lCharacter.isClimbing = true
          end

           -- Touches haut : Monter à l'échelle
          if ((love.keyboard.isDown(lCharacter.keys["moveUp"]) or love.keyboard.isDown(lCharacter.keys["moveUpAlt"])) and isOnLadder and not lCharacter.isJumping) then
            lCharacter.vY = (-math.floor(lCharacter.initialValues.speed / 4)) -- on grimpe 2 fois moins vite que la vitesse normale
            lCharacter.isClimbing = true
            lCharacter.direction = SPRITE_MOVE_UP
          end

           -- Touches bas : Descendre à l'échelle
          if ((love.keyboard.isDown(lCharacter.keys["moveDown"]) or love.keyboard.isDown(lCharacter.keys["moveDownAlt"])) and isOnLadder) then
            lCharacter.vY = (math.floor(lCharacter.initialValues.speed / 4)) -- on descend 2 fois moins vite que la vitesse normale
            lCharacter.isClimbing = true
            lCharacter.direction = SPRITE_MOVE_DOWN
          end

          -- Vérifie si le joueur ne grimpe plus
          if (not isOnLadder and lCharacter.gravity == 0 and not lCharacter.isJumping) then
            lCharacter.gravity = lCharacter.initialValues.gravity
          end

          -- Vérifie si le joueur est prêt pour le saut suivant ?
          if (not (love.keyboard.isDown(lCharacter.keys["moveUp"]) and not love.keyboard.isDown(lCharacter.keys["moveUpAlt"])) and not lCharacter.canJump and lCharacter.isStanding) then
            lCharacter.canJump = true
          end
        end -- if (not lCharacter.checkIfCanCollideWithTiles()) then
      end --if (lCharacter.moveWithKeys and lCharacter..checkIfActive()) then

      -- seul le joueur utilise la friction et l'angle de rotation pour le calcul des vitesses
      if (lCharacter.useFriction) then

        -- Calcul des vélocités
        -- ******************************
        -- prise en compte de la friction de l'environnement et du poids
        local frictionAndWeight = lCharacter.weight * lCharacter.friction * pDt
        if (lCharacter.vX > 0) then
          lCharacter.vX = lCharacter.vX - frictionAndWeight
        elseif (lCharacter.vX < 0) then
          lCharacter.vX = lCharacter.vX + frictionAndWeight
        end
        if (lCharacter.vY > 0) then
          lCharacter.vY = lCharacter.vY - frictionAndWeight
        elseif (lCharacter.vY < 0) then
          lCharacter.vY = lCharacter.vY + frictionAndWeight
        end
        if (math.abs(lCharacter.vX) < 0.01) then lCharacter.vX = 0 end
        if (math.abs(lCharacter.vY) < 0.01) then lCharacter.vY = 0 end
      end -- if (useFriction) then
    end -- if (appState.currentScreen.name == SCREEN_PLAY) then
  end -- character.update

  --- Vérifie les collisions (override)
  -- @return SPRITE_COLLISION_LIMIT_LEFT, SPRITE_COLLISION_LIMIT_RIGHT ,SPRITE_COLLISION_LIMIT_TOP, SPRITE_COLLISION_LIMIT_BOTTOM ou SPRITE_COLLISION_NONE
  function lCharacter.collide ()
    ---- debugFunctionEnterNL("character.collide") -- ATTENTION cet appel peut remplir le log

    if (not lCharacter.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end
    local collisionType = SPRITE_COLLISION_NONE
    local xMin, yMin, xMax, yMax = lCharacter.getScreenLimits()
    if (lCharacter.x < xMin) then
      lCharacter.x = xMin
      lCharacter.vX = 0
      collisionType = SPRITE_COLLISION_LIMIT_LEFT
    elseif (lCharacter.x > xMax) then
      lCharacter.x = xMax
      lCharacter.vX = 0
      collisionType = SPRITE_COLLISION_LIMIT_RIGHT
    end
    if (lCharacter.y < yMin) then
      lCharacter.y = yMin
      lCharacter.vY = 0
      collisionType = SPRITE_COLLISION_LIMIT_TOP
    elseif (lCharacter.y > yMax) then
      lCharacter.y = yMax
      lCharacter.vY = 0
      collisionType = SPRITE_COLLISION_LIMIT_BOTTOM
    end
    return collisionType
  end -- character.collide

  -- Autres fonctions
  -- ******************************

  --- Le joueur effectue une action (ie. tire un projectile)
   -- @param pType (OPTIONNEL) type de l'action (voir les constantes LAST_EXT_ACTIONXXX). Si absent, la valeur par défaut sera utilisée.
 -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lCharacter.spriteAction = lCharacter.action
  function lCharacter.action (pType)
    -- debugFunctionEnter("character.action ", pType)
    lCharacter.spriteAction(pType)

    if (not lCharacter.checkIfCanMakeAction()) then return end

  end -- character.action

  --- Le joueur saute
  function lCharacter.jump ()
    debugFunctionEnter("character.jump")
    lCharacter.isJumping = true
    lCharacter.updateLastStanding(false)
    lCharacter.canJump = false
    lCharacter.gravity = lCharacter.initialValues.gravity
    lCharacter.vY = lCharacter.velocityJump
    lCharacter.playSound("Jump")
    lCharacter.playAnimation(ANIM_NAME_MOVE_JUMP)
  end -- character.jump

  --- Interactions de l'objet avec le clavier
  -- @param pScancode (OPTIONNEL) le scancode représentant la touche pressée
  -- @param pIsrepeat (OPTIONNEL) TRUE si cet événement keypress est une répétition. Le délai entre les répétitions des touches dépend des paramètres système de l'utilisateur
  function lCharacter.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("character.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    if (pKey == lCharacter.keys.action1) then lCharacter.action(LAST_EXT_ACTION1) end
    if (pKey == lCharacter.keys.action2) then lCharacter.action(LAST_EXT_ACTION2) end

    -- IMPORTANT
    -- ON NE TRAITE ICI QUE LES TOUCHES QUI NE SONT PAS EN RAPPORT AVEC LES MOUVEMENT DU JOUEUR
    -- CES DERNIÈRES SONT ET DOIVENT ÊTRE TRAITÉES DANS LA FONCTION SPRITE.UPDATEPLAYER())

    -- Activer les déplacements avec le clavier (ON/OFF).
    if (DEBUG_MODE and pKey == settings.appKeys.moveWithKeys) then
      local state = not lCharacter.moveWithKeys
      debugMessage("character.moveWithKeys=", state)
      lCharacter.moveWithKeys = state
    end
    -- Activer les déplacements avec la souris (ON/OFF).
    if (DEBUG_MODE and pKey == settings.appKeys.moveWithMouse) then
      local state = not lCharacter.moveWithMouse
      debugMessage("character.moveWithMouse=", state)
      lCharacter.moveWithMouse = state
    end
  end -- character.keypressed

  --- Interactions de l'objet avec la souris
  -- @param pX (OPTIONNEL) la position de la souris sur l'axe des x.
  -- @param pY (OPTIONNEL) la position de la souris sur l'axe des y.
  -- @param pDX (OPTIONNEL) La quantité a été déplacée le long de l'axe des abscisses depuis la dernière fois que la fonction a été appelée.
  -- @param pDY (OPTIONNEL) La quantité a évolué le long de l'axe des y depuis la dernière fois que la fonction a été appelée.
  -- @param pIstouch (OPTIONNEL) Vrai si le bouton de la souris appuie sur la touche à partir d'une touche tactile à l'écran tactile.
  function lCharacter.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("character.mousemoved ", pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    if (appState.currentScreen == SCREEN_PLAY) then
      if (lCharacter.moveWithMouse) then
        -- déplacements avec la souris
        -- lCharacter.x = lCharacter.pX - width / 2
        -- lCharacter.y = lCharacter.pY - height / 2
        lCharacter.vX = lCharacter.vX + lCharacter.speed * settings.mouse.sensibilityX * pDX
        lCharacter.vY = lCharacter.vY + lCharacter.speed * settings.mouse.sensibilityY * pDY
        -- debugMessage("pDX,pDY=", pDX ,pDY, "VX,VY=", lCharacter.vX ,lCharacter.vY
      end
    end -- if (appState.currentScreen == SCREEN_PLAY) then
  end -- character.mousemoved

  --- Intercepte le clic de souris
  -- @param pX position x de la souris, en pixels.
  -- @param pY position y de la souris, en pixels.
  -- @param pButton L'index du bouton qui a été pressé: 1 est le bouton principal, 2 est le bouton secondaire et 3 est le bouton du milieu.
  -- @param pIstouch (OPTIONNEL) Vrai si le bouton est une touche tactile.
  function lCharacter.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("character.mousepressed ", pX, pY, pButton, pIstouch)
  end -- character.mousepressed

  --- Incrémente le score
  -- @param pValue (OPTIONNEL) valeur à ajouter
  -- @return le score mis à jour
  function lCharacter.addScore (pValue)
    if (pValue == nil) then pValue = 1 end
    lCharacter.score = lCharacter.score + pValue
    return lCharacter.score
  end -- character.addScore

  --- Incrémente le niveau
  -- @param pValue (OPTIONNEL) valeur à ajouter
  -- @return le niveau mis à jour
  function lCharacter.addLevel (pValue)
    if (pValue == nil) then pValue = 1 end
    lCharacter.level = lCharacter.level + pValue
    return lCharacter.level
  end -- character.addLevel

  -- initialisation par défaut
  lCharacter.initialize()

  -- Ajout du player à la liste
  if (entities.characters ~= nil) then
    local playerIndex = #entities.characters + 1
    if (appState.currentPlayerId == playerIndex) then
      entities.characters[playerIndex] = nil -- nécessaire pour ne pas ajouter le joueur courant en doublon
    end
    lCharacter.id = playerIndex
    entities.characters[lCharacter.id] = lCharacter
  end

  return lCharacter, playerIndex
end -- createCharacter