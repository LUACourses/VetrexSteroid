-- Entité représentant la zone de jeu
-- ****************************************

--- Crée un pseudo objet de type viewport
-- @return un pseudo objet viewport
function createViewPort ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createViewPort")

  local lVP = {}

  --- Initialise l'objet
  function lVP.initialize ()
    debugFunctionEnter("viewport.initialize")

    -- position X minimale
    lVP.Xmin = 0
    -- position X maximale
    lVP.Xmax = love.graphics.getWidth()
    -- position Y minimale
    lVP.Ymin = 0
    -- position Y maximale
    lVP.Ymax = love.graphics.getHeight()
    -- largeur de caractère
    lVP.charWidth  = 10 -- par défaut, actualisé lors de l'initialisation du HUD
    -- hauteur de caractère
    lVP.charHeight = 15 -- par défaut, actualisé lors de l'initialisation du HUD
    -- couleur de fond par défaut
    lVP.backgroundColor = {0, 0, 50}
    -- couleur de texte par défaut
    lVP.textColor = {255, 255, 255}
    -- le viewport a t'il déjà été dessiné ?
    lVP.firstDrawDone = false

    -- cache le curseur de la souris
    love.mouse.setVisible(false)
    -- par défaut confine la souris dans la fenêtre, sauf en mode debug
    if (DEBUG_MODE == 0) then love.mouse.setGrabbed(true) end
  end -- viewport.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  function lVP.getWidth ()
    return math.floor((lVP.Xmax - lVP.Xmin) / DISPLAY_SCALE_X)
  end
  function lVP.getHeight ()
    return math.floor((lVP.Ymax - lVP.Ymin) / DISPLAY_SCALE_Y)
  end

  -- Autres fonctions
  -- ******************************

  --- Détruit l'objet
  function lVP.destroy ()
    debugFunctionEnter("viewport.destroy")
    lVP.clean()
    lVP = nil
  end -- viewport.destroy

  --- Effectue des nettoyages en quittant le jeu
  function lVP.clean ()
    debugFunctionEnter("viewport.clean")
    -- affiche le curseur de la souris
    love.mouse.setVisible(true)
  end -- viewport.clean

  --- Dessine l'objet
  function lVP.draw ()
    ---- debugFunctionEnterNL("viewport.draw") -- ATTENTION cet appel peut remplir le log
    -- utilise la couleur de fond définie pour remplir la zone de jeu
    love.graphics.setColor(lVP.backgroundColor[1], lVP.backgroundColor[2], lVP.backgroundColor[3])
    love.graphics.rectangle("fill", lVP.Xmin / DISPLAY_SCALE_X, lVP.Ymin / DISPLAY_SCALE_Y, lVP.Xmax / DISPLAY_SCALE_X, lVP.Ymax / DISPLAY_SCALE_Y)
    love.graphics.setColor(lVP.textColor[1], lVP.textColor[2], lVP.textColor[3])
    love.graphics.setColor(255, 255, 255, 255) -- RAZ des couleur
    lVP.firstDrawDone = true
  end -- viewport.draw

  -- initialisation par défaut
  lVP.initialize()
  return lVP
end -- createViewPort
