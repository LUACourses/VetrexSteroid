-- Entité représentant un écran de jeu
-- ****************************************
-- TYPE D'ECRAN
SCREEN_UNDEFINED = "screenUndefined"

--- Création de l'écran
-- @param pName nom de l'écran. Si absent, la valeur SCREEN_UNDEFINED sera utilisée
-- @param pBgImage (OPTIONNEL) image de fond.
-- @param pMusic (OPTIONNEL) musique
-- @return un pseudo objet screen
function createScreen (pName, pBgImage, pMusic)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createScreen ", pName, pBgImage, pMusic)
  if (pName == nil) then pName = SCREEN_UNDEFINED end

  local lScreen = {}
  -- index du sprite dans la table entities.sprites (utile pour son update et sa suppression)
  lScreen.listIndex = 0 -- mis à jour lors de l'ajout entities.sprites, ne pas remettre à 0 dans l'initialize
  -- nom de l'écran
  lScreen.name = pName
  -- image de fond
  lScreen.bgImage = nil -- modifié dans le initialize
  -- musique
  lScreen.music = nil -- modifié dans le initialize
  -- l'écran a t'il été initialisé
  lScreen.isInitialized = false
  -- liste des entrées de menu figurant sur l'écran
  lScreen.menuEntries = {}
  -- entrée de menu sélectionnée
  lScreen.menuSelection = 1

  --- Initialise l'écran
  -- @param pBgImage (OPTIONNEL) image de fond. Si absent, un fichier png ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  -- @param pMusic (OPTIONNEL) musique. Si absent, un fichier mp3 ayant le même nom que celui de l'écran sera recherché dans le dossier des images.
  function lScreen.initialize (pBgImage, pMusic)
    debugFunctionEnter("screen.initialize")
    debugInfos = ""

    local tempName
    if (pBgImage == nil) then
      tempName = FOLDER_IMAGES.."/"..lScreen.name..".png"
      lScreen.bgImage  = resourceManager.getImage(tempName)
    end
    if (pMusic == nil) then
      tempName = FOLDER_MUSICS.."/"..lScreen.name..".mp3"
      lScreen.music  = resourceManager.getSound(tempName,"stream")
    end

    -- uniquement dans les classe filles lScreen.isInitialized = true
  end -- screen.initialize

  --- affiche l'écran
  function lScreen.show ()
    debugFunctionEnter("screen.show")
    if (not lScreen.isInitialized) then lScreen.initialize() end

    -- on switch la musique
    musicManager.pause()
    if (appState.currentScreen ~= nil and appState.currentScreen.music ~= nil) then appState.currentScreen.music:stop() end
    if (lScreen.music ~= nil) then lScreen.music:play() end

    appState.currentScreen = lScreen
  end -- screen.show

  --- Actualise l'écran
  -- @param pDt delta time
  function lScreen.update (pDt)
    ---- debugFunctionEnterNL("screen.update") -- ATTENTION cet appel peut remplir le log
    -- rien à faire pour le moment
  end -- screen.update

  --- Dessine l'écran
  function lScreen.draw ()
    ---- debugFunctionEnterNL("screen.draw") -- ATTENTION cet appel peut remplir le log
    local color, content, font, textWidth
    love.graphics.push()
    love.graphics.scale(DISPLAY_SCALE_X, DISPLAY_SCALE_X)
    if (lScreen.bgImage ~= nil) then HUD.drawImageScaleCenter(lScreen.bgImage) end

    -- AFFICHAGE du message "automatique" (avec timer)
    -- ******************************
    -- note: doit être positionné APRES l'affichage de l'image de fond
    HUD.DisplayAutoHideMessage(6)

    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.pop()
  end -- screen.draw

  --- Gestion du "keypressed" sur l'écran
  -- @param TODO: description
  function lScreen.keypressed (pKey, pScancode, pIsrepeat)
    ---- debugFunctionEnterNL("screen.keypressed ", pKey, pScancode, pIsrepeat) -- ATTENTION cet appel peut remplir le log
    -- rien ici, tout est fait dans les classes filles
  end -- screen.keypressed

  --- Gestion du "mousepressed" sur l'écran
  -- @param TODO: description
  function lScreen.mousepressed (pX, pY, pButton, pIstouch)
    debugFunctionEnter("screen.mousepressed")
    -- rien ici, tout est fait dans les classes filles
  end -- screen.mousepressed

  --- Gestion du "mousemoved" sur l'écran
  -- @param TODO: description
  function lScreen.mousemoved (pX, pY, pDX, pDY, pIstouch)
    ---- debugFunctionEnterNL("screen.mousemoved ", pX, pY, pDX, pDY, pIstouch) -- ATTENTION cet appel peut remplir le log
    -- rien ici, tout est fait dans les classes filles
  end -- screen.mousemoved

  --- Affichage du menu de l'écran
  -- @param pOffsetX (OPTIONNEL) décalage en X du menu par rapport au centre de l'écran. Si absent, la valeur 0 sera utilisée.
  -- @param pOffsetY (OPTIONNEL) décalage en Y du menu par rapport au centre de l'écran. Si absent, la valeur 0 sera utilisée.
  -- @param pFont (OPTIONNEL) police d'affichage. Si absent, la police courante sera utilisée.
  -- @param pColor (OPTIONNEL) couleur d'affichage.
  function lScreen.menuDraw (pOffsetX, pOffsetY, pFont, pColor)
    ---- debugFunctionEnterNL("screen.menuDraw ", pOffsetX, pOffsetY, pFont) -- ATTENTION cet appel peut remplir le log
    local i, font, h
    if (pFont == nil) then font = love.graphics.getFont() else font = pFont end
    h = font:getHeight(" ")
    for i = 1, #lScreen.menuEntries do
      content = lScreen.menuEntries[i].text
      if (i == lScreen.menuSelection) then content = "> "..content.." <" end
      -- rappel: lHUD.displayTextScale (pText, pFont, pPosition, pOffsetX, pOffsetY, pColor, pTextWidth, pTextHeight)

      HUD.displayTextScale(content, font, POS_CENTER, pOffsetX, pOffsetY, pColor)
      pOffsetY = pOffsetY + h
    end -- for
  end -- screen.menuDraw

  --- Navigation dans le menu de l'écran
  -- @param pKey: touche du clavier enfoncée. Si absent, la valeur DUMMY_VALUE sera utilisée.
  function lScreen.menuNavigation (pKey)
    ---- debugFunctionEnterNL("screen.menuNavigation ", pKey) -- ATTENTION cet appel peut remplir le log
    if (pKey == nil) then pKey = DUMMY_VALUE end
    if (lScreen.menuEntries == nil or lScreen.menuEntries == {}) then return end
    if (pKey == settings.appKeys.menuUp) then
      lScreen.menuSelection = (lScreen.menuSelection - 2) % (#lScreen.menuEntries) + 1
    elseif (pKey == settings.appKeys.menuDown) then
      lScreen.menuSelection = (lScreen.menuSelection) % (#lScreen.menuEntries) + 1
    elseif (pKey == settings.appKeys.menuValid) then
      lScreen.menuEntries[lScreen.menuSelection].action()
    elseif (pKey == settings.appKeys.quitGame) then
      lScreen.menuEntries[lScreen.menuSelection].action()
    end
  end -- screen.menuNavigation

  -- l'initialisation par défaut
  lScreen.initialize(pBgImage, pMusic)

  -- Ajout de l'écran à la liste
  if (entities.screens ~= nil) then
    local screenIndex = #entities.screens + 1
    lScreen.tableIndex = screenIndex
    entities.screens[screenIndex] = lScreen
  end

  return lScreen
end -- createScreen
