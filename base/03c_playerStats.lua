-- Entité représentant les stats du jeu
-- ****************************************
-- inspiré du code crée par Periklis Ntanasis <pntanasis@gmail.com> 2014

--- Crée un pseudo objet de type gameStats
-- @return un pseudo objet gameStats
function createGameStats ()

  local pGameStats = {}

  pGameStats.content = {}
  pGameStats.fileName = "gameStats.txt"

  function pGameStats.readEntries ()
    pGameStats.content = {}
    if love.filesystem.exists(pGameStats.fileName) then
      local file = love.filesystem.newFile(pGameStats.fileName)
      file:open("r")
      for line in file:lines() do
        local entry = {}
        local temp = line:split('*')
        if (#temp == 3) then
          entry.score = temp[1]
          entry.date = temp[2]
          entry.nickname = temp[3]
          pGameStats.content[#pGameStats.content + 1] = entry
        end
      end
      file:close()
    end
  end

  function pGameStats.addScore (score, nickname)
    local sDate = os.date("%d-%m-%Y")
    local newEntries = {}
    local i = 1
    if (#pGameStats.content == 0) then
      table.insert(newEntries, {['score'] = score, ['date'] = sDate, ['nickname'] = nickname})
    else
      while (i <= #pGameStats.content and i <= 10) do
        if (tonumber(pGameStats.content[i].score) < score) then
          table.insert(newEntries, {['score'] = score, ['date'] = sDate, ['nickname'] = nickname})
          break
        else
          table.insert(newEntries, pGameStats.content[i])
        end
        i = i + 1
      end
      while (i <= #pGameStats.content + 1 and i < 10) do
        table.insert(newEntries, pGameStats.content[i])
        i = i + 1
      end
      if (i < 10 and #pGameStats.content == #newEntries) then
        table.insert(newEntries, {['score'] = score, ['date'] = sDate, ['nickname'] = nickname})
      end
    end
    pGameStats.content = nil
    pGameStats.content = newEntries
  end

  function pGameStats.getScoreEntries()
    return pGameStats.content
  end

  function pGameStats.saveScore ()
    local fileName = pGameStats.fileName
    local content = ""
    for i = 1, #pGameStats.content do
      content = content .. pGameStats.content[i].score..'*'..pGameStats.content[i]['date']..'*'..pGameStats.content[i]['nickname']..'\n'
    end
    if (string.len(content) > 0) then
      local result, message
      result, message = love.filesystem.write(fileName, content)
      if (result) then
        -- OK
      else
        logError("Impossible d'enregistrer les statistiques dans le fichier ".. fileName..".L'erreur suivante est survenue:"..message)
      end
    end
  end

  function pGameStats.isHighscore (score)
    if (score <= 0) then return false end

    if (#pGameStats.content < 10) then
      return true
    end
    for i = 1, #pGameStats.content do
      if (tonumber(pGameStats.content[i].score) < score) then
        return true
      end
    end
    return false
  end

  return pGameStats
end