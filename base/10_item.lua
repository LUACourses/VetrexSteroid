-- Entité représentant un élément de décor
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
SPRITE_TYPE_ITEM = SPRITE_TYPE..SPRITE_EXT_ITEM -- élément de décor
SPRITE_TYPE_ITEM_COLLECT = SPRITE_TYPE_ITEM..NEXT_EXT_COLLECT -- élément de décor collectable
SPRITE_TYPE_ITEM_EXIT = SPRITE_TYPE_ITEM..NEXT_EXT_EXIT -- sortie du niveau, passage au niveau suivant si touché et ouverte

--- Création d'élément de décor
-- @param pType (OPTIONNEL) type d'objet (voir les constantes SPRITE_TYPE_ITEM_XXX). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale.  Si absent la valeur par défaut des sprites sera utilisée.
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pWeight (OPTIONNEL) poids de l'objet. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur 1 sera utilisée.
-- @return un pseudo objet item
function createItem (pType, pImage, pX, pY, pWeight, pLife, pEnergy)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createItem ", pType, pX, pY, pWeight, pLife, pEnergy)

  -- rappel: createSprite(pImage, pX,pY,pvX,pvY,pType,pWeight,pLife,pEnergy,pPoints,pCanCollideWithTile,pVelocityMax,pVelocityJump,pGravity,pCanMakeAction,pStatus,pMustDestroyedAtBorder,pTimeToLive,pMustDestroyedOnCollision,pSpeed,pMustRespawnIfDestroyed)
  local lItem = createSprite(pImage, pX, pY, nil, nil, pType, pWeight, pLife, pEnergy)

  --- Initialise l'objet
  function lItem.initialize ()
    -- debugFunctionEnter("item.initialize ")
    -- rien ici, tout est fait dans les classes filles
  end -- item.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --- affiche l'objet
  --[[ à décommenter pour des comportements spécifiques
  lItem.spriteDraw = lItem.draw -- mémorise la fonction du parent (sprite) afin de l'appeler si nécessaire
  function lItem.draw ()
    ---- debugFunctionEnterNL("item.draw") -- ATTENTION  cet appel peut remplir le log
    -- affiche l'objet comme un sprite normal
     lItem.spriteDraw()
  end -- item.draw
  ]]

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, la valeur true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lItem.spriteDestroy = lItem.destroy -- mémorise la fonction du parent (sprite) afin de l'appeler si nécessaire
  function lItem.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    debugFunctionEnter("item.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

    if (pMustAddScore == nil) then pMustAddScore = true end
    if (pMustShowFX == nil) then pMustShowFX = lItem.mustDestroyedWithFX end
    if (pMustPlaySound == nil) then pMustPlaySound = lItem.mustDestroyedWithSound end

    if (pMustAddScore) then
      player.addScore(lItem.points)
    end
    if (pMustShowFX) then
      createEffect(SPRITE_TYPE_EFFECT_EXPLODE, lItem.x, lItem.y, lItem.width, lItem.height)
    end
    if (pMustPlaySound) then
      lItem.playSound("Destroy")
    end
    -- la destruction effective se fait dans la classe parent
    lItem.spriteDestroy()
  end -- item.destroy

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lItem.spriteUpdate = lItem.update
  function lItem.update (pDt)
    ---- debugFunctionEnterNL("item.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lItem.spriteUpdate(pDt)
    -- déplace l'objet avec la caméra
    if ((camera.vX > 0 or camera.vY > 0) and lItem.checkIfMoveWithCamera()) then
      lItem.x = lItem.x + camera.vX * pDt
      lItem.y = lItem.y + camera.vY * pDt
    end
  end -- item.update

  --- Vérifie les collisions de l'objet (override)
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  --[[ à décommenter pour des comportements spécifiques
  function lItem.collide ()
    ---- debugFunctionEnterNL("item.collide") -- ATTENTION cet appel peut remplir le log
  end -- item.collide
  ]]

  -- initialisation par défaut
  lItem.initialize()

  return lItem
end -- createItem
