-- Entité représentant une trajectoire
-- ****************************************

-- TYPE DE TRAJECTOIRE
-- NOTE: la décomposition des constantes est importante dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
NAVPATH_TYPE = "pathType"
NAVPATH_TYPE_TOWARD = NAVPATH_TYPE..SEP_LAST.."toward"
NAVPATH_TYPE_TOWARD_GRAVITY = NAVPATH_TYPE..SEP_LAST.."towardGravity"
NAVPATH_TYPE_TOWARD_ROTATION = NAVPATH_TYPE..SEP_LAST.."towardRotation"
NAVPATH_TYPE_FROMTO = NAVPATH_TYPE..SEP_LAST.."fromTo"
NAVPATH_TYPE_FROMTO_CIRCLE = NAVPATH_TYPE..SEP_LAST.."fromToCircle"
NAVPATH_TYPE_UP = NAVPATH_TYPE..LAST_EXT_UP
NAVPATH_TYPE_RIGHT = NAVPATH_TYPE..LAST_EXT_RIGHT
NAVPATH_TYPE_DOWN = NAVPATH_TYPE..LAST_EXT_DOWN
NAVPATH_TYPE_LEFT = NAVPATH_TYPE..LAST_EXT_LEFT
NAVPATH_TYPE_RIGHTDOWN = NAVPATH_TYPE..LAST_EXT_RIGHTDOWN
NAVPATH_TYPE_LEFTDOWN = NAVPATH_TYPE..LAST_EXT_LEFTDOWN

-- TODO: ajouter des type de trajectoires utilisant des navPoints

-- Création d'un objet path (trajectoire)
-- @param pType (OPTIONNEL) type de trajectoire à appliquer. Si absent, la valeur NAVPATH_TYPE_TOWARD sera utilisée.
-- @param pEntity entité (sprite) qui sera déplacés avec la trajectoire.
-- @param pStartPoint (OPTIONNEL) entité (sprite) de départ de la trajectoire. Si absent, pEntity sera utilisé.
-- @param pEndPoint (OPTIONNEL) entité (sprite) de destination de la trajectoire.
-- @return un pseudo objet path
function createNavPath (pType, pEntity, pStartPoint, pEndPoint)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createNavPath ", pType, pEntity, pStartPoint, pEndPoint)

  local lNavPath = {}

  -- index de la trajectoire dans la table entities.navPaths (utile pour son update et sa suppression)
  lNavPath.listIndex = 0 -- mis à jour lors de l'ajout entities.sprites, ne pas remettre à 0 dans l'initialize
  -- position courante en X
  lNavPath.x = 0
  -- position courante en Y
  lNavPath.y = 0
  -- vitesse courante en X
  lNavPath.vX = 0
  -- vitesse courante en Y
  lNavPath.vY = 0
  -- entité (sprite) qui sera déplacés avec la trajectoire.
  lNavPath.entity = pEntity
  -- entité (sprite) de départ de la trajectoire.
  lNavPath.startPoint = pStartPoint
  -- entité de destination de la trajectoire.
  lNavPath.endPoint = pEndPoint
  -- point de navigation devant être parcourus par la trajectoire. Il seront utilisé pour fixer le déplacement de l'objet pointé par pAttachPoint
  lNavPath.navPoints = {}
  -- vitesse de passage d'un navPoint à l'autre
  lNavPath.delai = 0
  -- navPoint courant
  lNavPath.currentNavPoint = 1

  --- Initialise le projectile
  function lNavPath.initialize ()
    debugFunctionEnter("path.initialize")
  if (pStartPoint == nil) then pStartPoint = pEntity end

    -- la direction du projectile n'est pas la même selon le type de jeu
    local gameIsSide = (settings.gameType:find(GAME_TYPE_SIDE) ~= nil)
    local gameIsTopDown = (settings.gameType:find(GAME_TYPE_TOPDOWN) ~= nil)

    -- par défaut, ajuste la position de départ du projectile en fonction des dimensions de son objet d'attachement
    if (pStartPoint ~= nil) then
      pEntity.x = pStartPoint.x + (pStartPoint.width - pEntity.width) / 2
      pEntity.y = pStartPoint.y + (pStartPoint.height - pEntity.height) / 2
    end

    if (pType == NAVPATH_TYPE_UP) then
      -- déplacement vers le haut
      -- ******************************
      -- pas d'utilisation des navPoints dans ce cas
      pEntity.vX = 0
      pEntity.vY = -pEntity.speed

    elseif (pType == NAVPATH_TYPE_RIGHT) then
      -- déplacement vers la droite
      -- ******************************
      -- pas d'utilisation des navPoints dans ce cas
      pEntity.vX = pEntity.speed
      pEntity.vY = 0

    elseif (pType == NAVPATH_TYPE_DOWN) then
      -- déplacement vers le bas
      -- ******************************
      -- pas d'utilisation des navPoints dans ce cas
      pEntity.vX = 0
      pEntity.vY = pEntity.speed

    elseif (pType == NAVPATH_TYPE_LEFT) then
      -- déplacement vers la gauche
      -- ******************************
      -- pas d'utilisation des navPoints dans ce cas
      pEntity.vX = -pEntity.speed
      pEntity.vY = 0

    elseif (pType == NAVPATH_TYPE_RIGHTDOWN) then
      -- déplacement vers la droite et vers le bas en demi-vitesse
      -- ******************************
      -- pas d'utilisation des navPoints dans ce cas
      pEntity.vX = pEntity.speed
      pEntity.vY = pEntity.speed / 2

    elseif (pType == NAVPATH_TYPE_LEFTDOWN) then
      -- déplacement vers la gauche et vers le bas en demi-vitesse
      -- ******************************
      -- pas d'utilisation des navPoints dans ce cas
      pEntity.vX = -pEntity.speed
      pEntity.vY = pEntity.speed / 2

    elseif (pType == NAVPATH_TYPE_TOWARD_GRAVITY or pType == NAVPATH_TYPE_TOWARD) then
      -- déplacement en direction du mouvement du sprite d'origine, avec ou sans gravité
      -- ******************************
      -- pas d'utilisation des navPoints dans ce cas
      if (pType == NAVPATH_TYPE_TOWARD) then pEntity.gravity = 0 else pEntity.gravity = pEntity.initialValues.gravity end
      if (pStartPoint.direction == SPRITE_MOVE_LEFT) then
        pEntity.vX = -pEntity.speed
        pEntity.vY = 0
      elseif (pStartPoint.direction == SPRITE_MOVE_RIGHT) then
        pEntity.vX = pEntity.speed
        pEntity.vY = 0
      elseif (pStartPoint.direction == SPRITE_MOVE_UP) then
        pEntity.vX = 0
        pEntity.vY = -pEntity.speed
      elseif (pStartPoint.direction == SPRITE_MOVE_DOWN) then
        pEntity.vX = 0
        pEntity.vY = pEntity.speed
      end

    elseif (pType == NAVPATH_TYPE_TOWARD_ROTATION) then
      -- le projectile est dirigé avec l'angle de rotation de la source
      -- ******************************
      -- pas d'utilisation des navPoints dans ce cas
      pStartPoint.actionAngle = math.rad(pStartPoint.rotation)
      vX = pEntity.speed * math.cos(pStartPoint.actionAngle)
      vY = pEntity.speed * math.sin(pStartPoint.actionAngle)
      pEntity.vX = vX
      pEntity.vY = vY

    elseif (pType == NAVPATH_TYPE_FROMTO) then
      -- le projectile est dirigé vers le sprite cible
      -- ******************************
    -- pas d'utilisation des navPoints dans ce cas
      pStartPoint.actionAngle = math.angle(pStartPoint.x, pStartPoint.y, pEndPoint.x, pEndPoint.y)
      vX = pEntity.speed * math.cos(pStartPoint.actionAngle)
      vY = pEntity.speed * math.sin(pStartPoint.actionAngle)
      pEntity.vX = vX
      pEntity.vY = vY

     elseif (pType == NAVPATH_TYPE_FROMTO_CIRCLE) then
      -- l'angle change en fonction de l'emplacement relatif du joueur
      -- ******************************
      local relativeAngle = math.angle(pStartPoint.x, pStartPoint.y, pEndPoint.x, pEndPoint.y)

      -- on borne les angles pour faciliter les comparaisons
      local angleIncrement = 0.2 + pEndPoint.level / 50
      -- on actualise l'angle de tir en fonction des positions relatives et de l'angle de tir
      if (relativeAngle > 0 and relativeAngle < 3.14 / 2) then
        pStartPoint.actionAngle = 0; angleIncrement = math.abs(angleIncrement)
      elseif (relativeAngle > 3.14 / 2 and relativeAngle < 3.14) then
        pStartPoint.actionAngle = 3.14 / 2; angleIncrement = math.abs(angleIncrement)
      elseif (relativeAngle > -3.14 / 2  and relativeAngle < 0) then
        pStartPoint.actionAngle = 0; angleIncrement = - math.abs(angleIncrement)
      elseif (relativeAngle > - 3.14 and relativeAngle < - 3.14 / 2) then
        pStartPoint.actionAngle = - 3.14 / 2 ; angleIncrement = - math.abs(angleIncrement)
      end

      -- 1 chance sur 10 de changer le sens de rotation du tir
      if (math.random(1, 10) > 9) then angleIncrement = -angleIncrement end

      pStartPoint.actionAngle = pStartPoint.actionAngle + angleIncrement
      vX = pEntity.speed * math.cos(pStartPoint.actionAngle)
      vY = pEntity.speed * math.sin(pStartPoint.actionAngle)
      pEntity.vX = vX
      pEntity.vY = vY
    end

  end -- path.initialize()

  --- actualise l'animation en cours
  -- @param pDt delta time
  -- @return le status de l'animation.
  function lNavPath.update (pDt)
    -- debugFunctionEnter("path.update ", pDt) -- ATTENTION cet appel peut remplir le log
    -- TODO: mettre à jour la trajectoire en fonction du navpoint
    -- TODO: mettre à jour l'objet parent
  end -- path.update

  -- Ajout du sprite à la liste
  if (entities.navPaths ~= nil) then
    -- index du path dans la table (utile pour sa suppression)
    lNavPath.listIndex = #entities.navPaths + 1
    entities.navPaths[lNavPath.listIndex] = lNavPath
  end

  -- initialisation par défaut
  lNavPath.initialize()

  return lNavPath
end -- createNavPath