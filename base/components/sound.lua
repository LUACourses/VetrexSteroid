-- Entité représentant une animation
-- ****************************************

-- ÉTAT POSSIBLES
SOUND_STATUS = "soundStatus"
SOUND_STATUS_NORMAL = SOUND_STATUS..SEP_LAST.."normal"
SOUND_STATUS_STOPPED = SOUND_STATUS..SEP_LAST.."stopped"
SOUND_STATUS_DESTROYING = SOUND_STATUS..SEP_LAST.."destroying"
SOUND_STATUS_DESTROYED = SOUND_STATUS..SEP_LAST.."destroyed"
SOUND_STATUS_NOTFOUND = SOUND_STATUS..SEP_LAST.."notFound"

--- Crée un pseudo objet de type son
-- @param pName Nom du son.
-- @param pFilename nom du fichier audio.
-- @param pEntity entité à laquelle est liée le son.
-- @param pIsLooping (OPTIONNEL) true pour jouer l'animation en boucle. Si absent, la valeur false sera utilisée.
-- @return un pseudo objet sound
function createSound (pName, pFilename, pEntity, pIsLooping)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createSound ", pName, pFilename, pEntity, pIsLooping)

  if (pIsLooping == nil) then pIsLooping = false end

  local lSound = {}

  -- index de l'animation dans la table entities.animations (utile pour sa suppression)
  lSound.listIndex = 0 -- mis à jour lors de l'ajout entities.animations, ne pas remettre à 0 dans l'initialize
  -- nom
  lSound.name = pName
  -- status
  lSound.status = SOUND_STATUS_NORMAL
  -- dossier contenant les images ET les sons par défaut
  lSound.filename = pFilename
  -- source audio (son)
  lSound.source = nil
  -- entité à laquelle est liée le son
  lSound.entity = pEntity

  --- Initialise l'objet
  function lSound.initialize ()
    debugFunctionEnter("sound.initialize ")
    lSound.load()
  end -- sound.initialize

  -- Accesseurs pour les attributs de cet objet
  -- ******************************

  -- Autres fonctions
  -- ******************************

  --- Détruit l'objet
  function lSound.destroy ()
    -- debugFunctionEnter("sound.destroy")
    lSound.status = SOUND_STATUS_DESTROYING
  end -- sound.destroy

  --- Effectue du nettoyage lié à l'objet en quittant le jeu
  function lSound.clean ()
    ---- debugFunctionEnterNL("sound.clean")
    lSound.status = SOUND_STATUS_DESTROYED
    -- suppression du son dans le parent
    if (lSound.entity ~= nil and lSound.entity.sounds ~= nil and lSound.entity.sounds[lSound.name] ~= nil) then lSound.entity.sounds[lSound.name] = nil end
  end -- sound.clean

  --- Joue le son
  function lSound.play ()
    -- debugFunctionEnter("sound.play")
    if (lSound.status == SOUND_STATUS_NORMAL or lSound.status == SOUND_STATUS_STOPPED) then
      lSound.stop()
      lSound.source:play()
      lSound.status = SOUND_STATUS_NORMAL
    end
  end -- sound.play

  --- Stoppe le son
  function lSound.stop ()
    -- debugFunctionEnter("sound.stop")
    if (lSound.status == SOUND_STATUS_NORMAL or lSound.status == SOUND_STATUS_STOPPED) then
      -- on stoppe son son
      lSound.source:stop()
      lSound.status = SOUND_STATUS_STOPPED
    end
  end -- sound.stop

  -- Charge les sons par défaut liés à l'animation
  -- @param pExt (OPTIONNEL) extension des fichier à prendre en compte. Si absent,".mp3" sera utilisé
  -- note: utilise le nom de l'animation en le décomposant pour retrouver le nom du fichier à associer
  function lSound.load ()
    -- debugFunctionEnter("sound.load")
    local filename = lSound.filename
    if (love.filesystem.isFile(filename)) then
      lSound.source = resourceManager.getSound(filename, "static")
      lSound.source:setLooping(lSound.soundIsLooping)
      lSound.status = SOUND_STATUS_NORMAL
    else
      lSound.status = SOUND_STATUS_NOTFOUND
    end
  end -- sound.load

  -- initialisation par défaut
  lSound.initialize()

    -- Ajout du son à la liste
  if (entities.sounds ~= nil) then
    -- index du son dans la table (utile pour sa suppression)
    lSound.listIndex = #entities.sounds + 1
    entities.sounds[lSound.listIndex] = lSound
  end

  return lSound, lSound.listIndex
end -- createAnimation