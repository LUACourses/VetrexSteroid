-- Fonctions pour la map du jeu VetrexSteroid
-- Utilise une forme d'héritage simpSle: gameMap qui inclus map
-- ****************************************

-- valeur de départ indiquant un choix aléatoire de la coordonnées de départ du spawn
RANDOM_START_VALUE = -60-- en dehors de l'ecran
-- délai entre les test pour faire un spawn
SPAWN_DELAI = 1
-- nombre d'étoiles pour le starfield en fond d'écran
MAP_STARS_COUNT = 400

--- Crée un pseudo objet de type gameMap
-- @param pLevel (OPTIONNEL) numéro de la map (niveau de jeu) associé à la map
-- @return un pseudo objet gameMap
function createGameMap (pLevel)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("pLevel ", pLevel)

  local lGameMap = createMap (pLevel)

  -- Override des fonctions de la map
  -- ******************************

  -- Charge et initialise  la map
  -- @param pLevel (OPTIONNEL) numéro de la map (niveau de jeu) associé à la map
  function lGameMap.initialize (pLevel)
    debugFunctionEnter("gameMap.initialize ", pLevel)

    if (pLevel == nil) then pLevel = 1 end
    lGameMap.mapIsOK = false

    -- numéro du niveau
    lGameMap.index = pLevel
    -- type de map
    lGameMap.type = MAP_TYPE_NOT_TILED
    -- nom associé à chaque map
    -- TODO: extraire le nom depuis le fichier décrivant la map
    lGameMap.names = {}
    -- tuiles utilisées par la map
    lGameMap.tiles = {}
    -- largeur des tuiles de la map
    lGameMap.tilewidth = 1 -- mettre 1 permet de garder la compatibilité avec les fonctions qui utilisent des map avec des tuiles
    -- hauteur des tuiles de la map
    lGameMap.tileheight = 1  -- mettre 1 permet de garder la compatibilité avec les fonctions qui utilisent des map avec des tuiles
        -- largeur des tuiles de la map
    lGameMap.visibleWidth = math.floor(viewport.getWidth() / lGameMap.tilewidth)
    -- hauteur des tuiles de la map
    lGameMap.visibleHeight = math.floor(viewport.getHeight() / lGameMap.tileheight)
    -- contenu de la map
    lGameMap.grid = {}
    -- position de départ du joueur
    lGameMap.playerStart = {
      col = 1,
      line = 1
    }
    -- objets disponibles dans le niveau
    lGameMap.items = {}

    -- ennemis à spawner dans le niveau
    lGameMap.enemies = {}

    -- nombre d'ennemis à afficher simultanément à l'écran
    lGameMap.enemiestoShow = 0 -- modifié en fonction du niveau
    -- nombre d'ennemis affiché à l'écran
    lGameMap.enemiesCount = 0 -- modifié en fonction du niveau
    -- nombre d'ennemis à détruire pour passer le niveau
    lGameMap.enemiesToDestroy = 0 -- modifié en fonction du niveau
    -- nombre d'ennemis détruits
    lGameMap.enemiesDestroyed = 0
    -- probabilité de spawn d'un nouvel ennemi
    lGameMap.enemiesSpawnProbability = 0 -- modifié en fonction du niveau
    -- timer entre 2 vérifications pour faire un spawn
    lGameMap.spawnTimer = SPAWN_DELAI -- important, permet de faire un spawn dès le début
    -- le 1er spawn a t'il été effectué ?
    lGameMap.spawnDone = false

    -- attributs liés au starfield
    lGameMap.width = viewport.getWidth()
    lGameMap.height = viewport.getHeight()

    -- starfield utilisé pour le fond d'écran (starfield)
    lGameMap.starField = {
      vX = 0,
      vY = 0,
      vXmin = 1,
      vXmax = 5,
      vYmin = 10,
      vYmax = 50,
      minX = 1,
      maxX = lGameMap.width ,
      minY = 1,
      maxY = lGameMap.height ,
      minSize = 1,
      maxSize = 3,
      startCount = MAP_STARS_COUNT,
      baseColor = {255, 255, 255, 255},
      -- facteur déplacement du starfield suivant la position du joueur. Mettre à 0 pour aucun
      moveWithPlayerSpeedFactor = 12,
      content = {}
    }

    -- si on a déjà chargé un niveau, efface toutes les entités
    resetEntities()

    lGameMap.mapIsOK = true -- important
    return lGameMap.mapIsOK
  end -- gameMap.initialize

  --- Crée et positionne les contenus (objets, PNJs...) dans la map
  -- @param pDt Delta time
  function lGameMap.update (pDt)
    ---- debugFunctionEnterNL("gameMap.update ", pDt) -- ATTENTION cet appel peut remplir le log

    -- on teste si le niveau est fini
    -- si le boss est détruit (OK, c'est automatique actuellement)
    -- le nombre d'ennemis à détruire est-il atteint ?
    if (lGameMap.enemiesToDestroy > 0 and lGameMap.enemiesDestroyed >= lGameMap.enemiesToDestroy) then
      -- oui, on passe au niveau suivant
      nextLevel()
      return
    end

      -- on utilise des alias pour optimiser les accès
    local mathAbs = math.abs
    local mathRandom = math.random

    -- Actualisation du starfield
    -- ******************
    local i
    local speedFactor = lGameMap.starField.moveWithPlayerSpeedFactor / pDt
    for i = 1, lGameMap.starField.startCount do
      local newStar = lGameMap.starField.content[i]

      -- déplacement de l'étoile
      newStar.x = newStar.x + newStar.vX * pDt
      newStar.y = newStar.y + newStar.vY * pDt

      -- déplacement du starfield avec la position du joueur
      if (lGameMap.starField.moveWithPlayerSpeedFactor ~= nil and lGameMap.starField.moveWithPlayerSpeedFactor ~= 0) then
        newStar.x = newStar.x + (player.vX / speedFactor)
        newStar.y = newStar.y + (player.vY / speedFactor)
      end

      -- les étoiles qui sortent de l'écran réapparaissent de l'autre coté
      if (newStar.x > lGameMap.starField.maxX) then
        newStar.x = lGameMap.starField.minX
      elseif (newStar.x < lGameMap.starField.minX) then
        newStar.x = lGameMap.starField.maxX
      end
      if (newStar.y > lGameMap.starField.maxY) then
        newStar.y = lGameMap.starField.minX
      elseif (newStar.y < lGameMap.starField.minY) then
        newStar.y = lGameMap.starField.maxY
      end
    end -- for

    -- Spawne l'ennemi suivant dans la liste
    -- ******************
    -- le délai pour spawner est il écoulé ?
    if (lGameMap.spawnTimer < SPAWN_DELAI) then
      lGameMap.spawnTimer = lGameMap.spawnTimer + pDt
    else

      lGameMap.spawnTimer = 0

      local spawnProbability
      -- spawner un ennemi dès le départ du niveau
      if (lGameMap.spawnDone) then
        spawnProbability = mathRandom()
      else
        lGameMap.spawnDone = true
        spawnProbability = 0
      end

      local ennemiesCount = #lGameMap.enemies

      -- doit on spawner un nouvel ennemi ?
      if (ennemiesCount > 0 and (spawnProbability < lGameMap.enemiesSpawnProbability)) then
        local enemy = lGameMap.enemies[ennemiesCount]

        -- on le supprime de la liste des ennemis à spawner
        lGameMap.enemies[ennemiesCount] = nil

        -- détermine une position de spawn dans l'écran et éloignée du joueur
        local spawnPosX, spawnPosY
        spawnPosX, spawnPosY = findSecurePosition (0, 0, (lGameMap.width - 50), (lGameMap.height - 50), 80, {player})

        -- on le place à la nouvelle position ou bien on garde la position d'origine
        if (enemy.x == RANDOM_START_VALUE) then enemy.x = spawnPosX end
        if (enemy.y == RANDOM_START_VALUE) then enemy.y = spawnPosY end

        -- on le "réveille"
        enemy.status = SPRITE_STATUS_NORMAL
        debugMessage("SPAWNE un "..enemy.type.. " at ".. spawnPosX..","..spawnPosY)

      end -- if (#lGameMap.enemies > 0 ) and (spawnProbability < lGameMap.enemiesSpawnProbability)) then


    end -- if (lGameMap.spawnTimer < 1 ) then
  end

  --- affiche la map
  function lGameMap.draw ()
    ---- debugFunctionEnterNL("gameMap.draw") -- ATTENTION cet appel peut remplir le log
    lGameMap.mapIsOK = true
      -- on utilise des alias pour optimiser les accès
    local mathRandom = math.random

    -- Affichage du starfield
    -- ******************
    local i

    for i = 1, lGameMap.starField.startCount do
      local color = {}
      local newStar = lGameMap.starField.content[i]
      color[1] = lGameMap.starField.baseColor[1] * mathRandom(0.7, 1)
      color[2] = lGameMap.starField.baseColor[2] * mathRandom(0.7, 1)
      color[3] = lGameMap.starField.baseColor[3]
      color[4] = lGameMap.starField.baseColor[4] * mathRandom(0.7, 1)
      love.graphics.setColor(color)
      love.graphics.circle("fill", newStar.x, newStar.y, newStar.size)
    end -- for
    love.graphics.setColor(255, 255, 255, 255)

    return lGameMap.mapIsOK
  end -- gameMap.draw

  --- Crée et positionne les contenus (objets, PNJs...) dans la map pour un niveau (de difficulté) donné
  -- @param pLevel (OPTIONNEL) niveau (de difficulté). Si absent, la valeur 1 sera utilisée.
  function lGameMap.createLevelContent (pLevel)
    debugFunctionEnter("gameMap.createLevelContent ", pLevel)
    if (pLevel == nil) then pLevel = 1 end

    debugMessage("createLevelContent ", pLevel)

    lGameMap.enemiesDestroyed = 0
    lGameMap.index = pLevel

    -- Génération du starfield
    -- ******************
    local i
    for i = 1, lGameMap.starField.startCount do
      local newStar = {
        vX = math.random(lGameMap.starField.vXmin, lGameMap.starField.vXmax),
        vY = math.random(lGameMap.starField.vYmin, lGameMap.starField.vYmax),
        size = math.random(lGameMap.starField.minSize, lGameMap.starField.maxSize),
        x = math.random(lGameMap.starField.minX, lGameMap.starField.maxX),
        y = math.random(lGameMap.starField.minY, lGameMap.starField.maxY)
      }
      lGameMap.starField.content[i] = newStar
    end -- for

    if (true) then -- mettre à false pour test uniquement
      local currentLevelFactor = pLevel / appState.levelToWin
      local difficultyFactor = 1.5 -- influence grandement le nombre d'ennemis affichés simultanément à l'écran et sur le nombre à détruire
      -- nombre d'ennemis à détruire pour passer le niveau
      lGameMap.enemiesToDestroy = math.floor(5 + pLevel * difficultyFactor * 2)
      --[[
        exemple de calcul pour difficultyFactor = 1: SEMBLE CORRECT
          niveau 1
            enemiesToDestroy = 5 + 1 * 1 * 2 = 7 ennemis à détruire
          niveau 2
            enemiesToDestroy = 5 + 2 * 1 * 2 = 9 ennemis à détruire
          niveau 10 (moitié du jeu)
            enemiesToDestroy = 5 + 10 * 1 * 2 = 25 ennemis à détruire
          niveau 20 (fin du jeu)
            enemiesToDestroy = 5 + 20 * 1 * 2 = 45 ennemis à détruire
      ]]

      -- nombre d'ennemis à afficher simultanément à l'écran
      lGameMap.enemiestoShow = math.floor(3 + pLevel * difficultyFactor)
      --[[
        exemple de calcul pour difficultyFactor = 2: SEMBLE TROP GRAND
          niveau 1
            enemiestoShow = 3 + 1 * 2 = 5 ennemis affichés simultanément
          niveau 2
            enemiestoShow = 3 + 2 * 2 = 7 ennemis affichés simultanément
          niveau 10 (moitié du jeu)
            enemiestoShow = 3 + 10 * 2 = 23 ennemis affichés simultanément
          niveau 20 (fin du jeu)
            enemiestoShow = 3 + 20 * 2 = 43 ennemis affichés simultanément

        exemple de calcul pour difficultyFactor = 1: SEMBLE CORRECT
          niveau 1
            enemiestoShow = 3 + 1 * 1 = 4 ennemis affichés simultanément
          niveau 2
            enemiestoShow = 3 + 2 * 1 = 5 ennemis affichés simultanément
          niveau 10 (moitié du jeu)
            enemiestoShow = 3 + 10 * 1 = 13 ennemis affichés simultanément
          niveau 20 (fin du jeu)
            enemiestoShow = 3 + 20 * 1 = 23 ennemis affichés simultanément
      ]]

      -- probabilité de spawn d'un nouvel ennemi
      local minProbability = 0.2
      local maxProbability = 0.8
      lGameMap.enemiesSpawnProbability = minProbability + (maxProbability - minProbability) * currentLevelFactor
      --[[
        exemple de calcul:
          niveau 1
            currentLevelFactor = 1/20 = 0.05
            (maxProbability - minProbability) * currentLevelFactor = 0.03
            proba: en 0.2 et 0.23 A TESTER
          niveau 2
            currentLevelFactor = 2/20 = 0.1
            (maxProbability - minProbability) * currentLevelFactor = 0.06
            proba: en 0.2 et 0.26 A TESTER
          niveau 10 (moitié du jeu)
            currentLevelFactor = 10/20 = 0.5
            (maxProbability - minProbability) * currentLevelFactor = 0.3
            proba: en 0.2 et 0.5 A TESTER
          niveau 10 (fin du jeu)
            currentLevelFactor = 20/20 = 1
            (maxProbability - minProbability) * currentLevelFactor = 0.6
            proba: en 0.2 et 0.8 OK
      ]]

      -- Spawning des pnj
      -- ******************

      -- création de la liste des ennemis à spawner
      -- différente valeur de base pour les probabilités d'apparition des type d'ennemis, classées par ordre de test successif (et par ordre de difficulté)
      -- TODO: déplacer cette valeur DANS la pseudo classe de chaque ennemi ?
      local npcBossProbability = 0.04 -- 1 chance sur 25 de spawner
      local npcBounceProbability = 0.2 -- 1 chance sur 5 de spawner
      local npcFallProbability = 0.33 -- 1 chance sur 3 de spawner
      -- NOTE: pas de probabilité pour le dernier car c'est celui qui sera spawné si aucun autre ne l'est

      local index, enemy
      local spawnPosX = RANDOM_START_VALUE
      local spawnPosY = RANDOM_START_VALUE
      for index = 1, lGameMap.enemiesToDestroy do
        -- définir un type d'ennemi parmi ceux possibles en fonction du niveau de jeu courant
        local newNpcProbability

        if (index == 1 or index == lGameMap.enemiesToDestroy) then
          -- le 1er et le dernier pnj sont des npcBounce
          newNpcProbability = npcBounceProbability
        else
          -- sinon, on utilise des probabilités aléatoires
          newNpcProbability = math.random()
        end
        -- création du type d'ennemi en fonction des probabilités

        -- rappel: createNpcXXX (pX, pY, pWeight, pLife, pEnergy, pImage, pSubType)
        if (newNpcProbability <= npcBossProbability) then
          npcEnergy = 10 + lGameMap.index * 2 -- 12 tirs mini pour le détruire, augmente avec le niveau
          -- on change aléatoirement un des coordonnées la position de départ du sprite pour le faire partir depuis un coin de l'écran
          local startChange = math.random()
          if (startChange < 0.25) then
            spawnPosX = 10
          elseif (startChange < 0.5) then
            spawnPosX = lGameMap.width - 80
          elseif (startChange < 0.75) then
            spawnPosY = 10
          else
            spawnPosY = lGameMap.height - 80
          end
          enemy = createNpcBossBounce(spawnPosX, spawnPosY, 1, 1, npcEnergy)
        elseif (newNpcProbability <= npcBounceProbability) then
          npcEnergy = 5 + lGameMap.index -- 6 tirs mini pour le détruire, augmente avec le niveau
          enemy = createNpcBounce(spawnPosX, spawnPosY, 1, 1, npcEnergy)

        elseif (newNpcProbability <= npcFallProbability) then
          if (math.random() > 0.5) then
            npcEnergy = math.floor(5 + lGameMap.index / 2) -- 5 tirs mini pour le détruire, augmente avec le niveau
            enemy = createNpcFall(spawnPosX, 10, 1, 1, npcEnergy, nil, SPRITE_TYPE_NPC_FALL1)
          else
            npcEnergy = math.floor(5 + lGameMap.index / 2) -- 5 tirs mini pour le détruire, augmente avec le niveau
            enemy = createNpcFall(spawnPosX, 10, 1, 1, npcEnergy, nil, SPRITE_TYPE_NPC_FALL2)
          end

        else
          npcEnergy = math.floor(3 + lGameMap.index / 5) -- 3 tirs mini pour le détruire, augmente avec le niveau
          enemy = createNpcFixed(spawnPosX, spawnPosY, 1, 1, npcEnergy)
        end -- if (probability <= bossProbability) then

        -- ajoute l'ennemi à la liste des ennemis du niveau
        lGameMap.enemies[#lGameMap.enemies + 1] = enemy
        -- debugMessage("enemies["..#lGameMap.enemies.."] est un "..enemy.type)
      end -- for

    else
      -- spawning pour tests uniquement
      --[[
      enemy = createNpcBounce(0, 0, 1, 1, 10)
      enemy.spawnToMap(300, 20)
      enemy = createNpcFixed(0, 0, 1, 1, 3)
      enemy.spawnToMap(lGameMap.width / 2 , lGameMap.height / 2 - 150)
      enemy = createNpcFixed(0, 0, 1, 1, 3)
      enemy.spawnToMap(lGameMap.width / 2 , lGameMap.height / 2 + 100)
      enemy = createNpcFixed(0, 0, 1, 1, 3)
      enemy.spawnToMap(lGameMap.width / 2 - 300 , lGameMap.height / 2 - 150)
      enemy = createNpcFixed(0, 0, 1, 1, 3)
      enemy.spawnToMap(200 , lGameMap.height / 2 + 300)
      enemy = createNpcFixed(0, 0, 1, 1, 3)
      enemy.spawnToMap(900 , lGameMap.height / 2 - 300)

      enemy = createNpcFall(0, 0, 1, 1, 10, nil, SPRITE_TYPE_NPC_FALL2)
      enemy.spawnToMap(200, 20)
      enemy = createNpcFall(0, 0, 1, 1, 10, nil, SPRITE_TYPE_NPC_FALL1)
      enemy.spawnToMap(100, 20)

      enemy = createNpcBounce(0, 0, 1, 1, 10)
      enemy.spawnToMap(100, 200)
      enemy = createNpcBounce(0, 0, 1, 1, 10)
      enemy.spawnToMap(800, 400)
      enemy = createNpcBounce(0, 0, 1, 1, 10)
      enemy.spawnToMap(1000, 600)
      enemy = createNpcBounce(0, 0, 1, 1, 10)
      enemy.spawnToMap(200, 600)

      ]]
      enemy = createNpcBossBounce(0, 0, 1, 1, 1)
      enemy.spawnToMap(400, 20)
      enemy.canMakeAction = false
    end

    lGameMap.mapIsOK = true
    return lGameMap.mapIsOK
  end -- gameMap.createLevelContent

  -- Fonctions spécifiques à cette map
  -- ******************************

  lGameMap.initialize(pLevel)

  return lGameMap
end -- createGameMap