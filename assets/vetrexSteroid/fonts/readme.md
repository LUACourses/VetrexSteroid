- Ce dossier contient les polices du jeu.

- Certains fichiers doivent obligatoirement être présents car il sont utilisés par le framework. Voir la fonction HUD.initialize() pour plus d'infos.
  En voici la liste:
    content.ttf
    gui.ttf
    menuContent.ttf
    menuTitle.ttf
    normal.ttf