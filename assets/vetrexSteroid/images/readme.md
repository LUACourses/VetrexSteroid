- Ce dossier contient les images du jeu.

- Le fichier icon.png est l'icône associée au jeu pour le type de jeu indiqué.

- Il existe un dossier par type d'assets, ils doivent obligatoirement être présents car il sont utilisés par le framework, et chaque fichiers doit être placé dans le dossier adapté.
  En voici la liste non exhaustive:
    effects
    items
    players
    npcs
    projectiles
    ui

- à l'intérieur de chaque dossier, il existe un sous-dossier anims devant contenir les fichiers images ET AUDIO associés à chaque animation.
-
- Les sons par défaut qui peuvent être automatiquement associés aux animations doivent être présents dans ce sous-dossier sous la forme anim<ANIMATION>.mp3 pour le fichier joué pendant l'animation et <TYPE_D_ANIMATION>End.mp3 pour le fichier joué à la fin de l'animation. Pour plus d'infos, voir la fonction Animation.loadDefaultSounds().
  Voici une liste non exhaustive des noms de fichier possibles pouvant être chargé automatiquement:
    \assets\side_plateformer\images\players\anims\jump\animJumpEnd.mp3
    \assets\side_plateformer\images\players\anims\jump\animJump.mp3
    \assets\side_plateformer\images\players\anims\climb\animClimb.mp3
    \assets\side_plateformer\images\players\anims\fall\animFall.mp3

- Le dossier ui contient les images qui seront automatiquement chargées comme fond dans les écrans de jeu. Pour plus d'infos, voir le fichier main.lua.
  En voici la liste:
    screenEndLose.png
    screenEndWin.png 
    nextScreen.png   
    screenPause.png  
    screenPlay.png   
    screenStart.png  
