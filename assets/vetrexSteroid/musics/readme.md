- Ce dossier contient les musiques du jeu. 

- Certains fichiers doivent obligatoirement être présents car ils sont utilisés par le framework. Pour plus d'infos, voir le fichier main.lua.
  En voici la liste:
    screenEndLose.mp3
    screenEndWin.mp3 
    screenStart.mp3  

- Les musiques par défaut qui peuvent être automatiquement chargées à chaque niveau de jeu doivent être présentes dans ce dossier sous la forme mmLevel<numero>.mp3
  voici une liste non exhaustives des noms de fichier possibles pouvant être chargé automatiquement. Pour plus d'infos, voir le fichier main.lua.
    mmLevel1.mp3     
    mmLevel2.mp3     
    ...