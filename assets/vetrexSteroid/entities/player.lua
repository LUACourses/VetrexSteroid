-- Entité représentant le joueur
-- Utilise une forme d'héritage simpSle:  player qui inclus character qui inclus sprite
-- ****************************************

-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "player" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_CHARACTER_PLAYER = SPRITE_TYPE_CHARACTER..SEP_NEXT..SPRITE_TYPE_DETAIL -- tous les joueurs
SPRITE_TYPE_CHARACTER_PLAYER1 = SPRITE_TYPE_CHARACTER_PLAYER..LAST_EXT_1 -- le joueur courant

--- Création du joueur
-- @return un pseudo objet player
function createPlayer ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createPlayer")"

  local lPlayer = createCharacter (
    SPRITE_TYPE_CHARACTER_PLAYER1, -- pType
    0, -- pX
    0, -- pY
    nil, -- pWidth (calculé automatiquement en fonction de la map)
    nil, -- pHeight (calculé automatiquement en fonction de la map)
    FOLDER_PLAYER_IMAGES.."/player.ptxt", -- pImage
    0, -- pVelocityJump
    0, -- pSpeed
    50, -- pFriction
    0, -- pGravity
    250, -- pVelocityMax. NOTE: en augmentant ce paramètre on diminue la limite de vélocité et on augmente l'inertie du mouvement (par exemple on rend le sol moins adhérent)
    3, -- pWeight. NOTE: en augmentant ce paramètre on augmente la vitesse de chute et diminue la hauteur des sauts
    nil, -- pLifeImage
    1, -- pLevel, valeur restaurée par restorePlayer
    3, -- pLife
    0, -- pSCore, valeur restaurée par restorePlayer
    true, -- pUseFriction
    false, -- pMoveWithMouse
    nil, -- pMoveWithKeys
    nil, -- pColor (par défaut)
    0, -- pvX
    0, -- pvY
    false, -- pCanCollideWithTile
    false, -- pCanJump
    true, -- pUseVelocity (ajoute de l'inertie au mouvement)
    1, -- pEnergy
    true -- pMustRespawnIfDestroyed
  )

  --- Initialise l'objet
  function lPlayer.initialize ()
    debugFunctionEnter("player.initialize")

    lPlayer.direction = SPRITE_MOVE_UP
    lPlayer.color = {0, 128, 255, 255 }
    -- Ajoute les animations du joueur (si les images nécessaires sont présentes)
    -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    lPlayer.addAnimation(ANIM_NAME_ACT_ACTION1, FOLDER_PLAYER_ANIMS.."/action1", ".ptxt", nil, false, ANIM_NAME_MOVE_IDLE)
    lPlayer.addAnimation(ANIM_NAME_MOVE_IDLE, FOLDER_PLAYER_ANIMS.."/idle", ".ptxt")
    lPlayer.addAnimation(ANIM_NAME_STATE_DESTROY, FOLDER_PLAYER_ANIMS.."/destroy", ".ptxt", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_DESTROYING, lPlayer.clean)
    lPlayer.addAnimation(ANIM_NAME_STATE_LOSE_LIFE, FOLDER_PLAYER_ANIMS.."/loselife", ".ptxt", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_ANIM_RUNNING, SPRITE_STATUS_NORMAL, lPlayer.checkLife)
    local lAnimation = lPlayer.addAnimation(ANIM_NAME_STATE_SPAWN, FOLDER_PLAYER_ANIMS.."/spawn", ".ptxt", nil, false, ANIM_NAME_MOVE_IDLE, nil, nil, nil, nil, nil, SPRITE_STATUS_INVINCIBLE, SPRITE_STATUS_NORMAL)
    lAnimation.color = {128, 128, 255, 255}
   	lPlayer.playAnimation(ANIM_NAME_MOVE_IDLE)
  end -- player.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --[[ à décommenter pour des comportements spécifiques
  --- affiche l'objet
  lPlayer.characterDraw = lPlayer.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lPlayer.draw ()
    ---- debugFunctionEnterNL("player.draw") -- ATTENTION  cet appel peut remplir le log
    -- affiche l'objet comme un sprite normal
     lPlayer.characterDraw()
  end -- player.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, la valeur true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lPlayer.characterDestroy = lPlayer.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lPlayer.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    debugFunctionEnter("player.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

    -- la destruction effective se fait dans la classe parent
    lPlayer.characterDestroy()
  end -- player.destroy

  ]]
  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lPlayer.characterUpdate = lPlayer.update
  function lPlayer.update (pDt)
    ---- debugFunctionEnterNL("player.update ", pDT) -- ATTENTION cet appel peut remplir le log
    -- lPlayer.characterUpdate(pDt)
    -- on utilise des mouvements spécifiques pour ce type de joueur
     lPlayer.spriteUpdate(pDt)

    if (lPlayer.status == SPRITE_STATUS_DESTROYING or lPlayer.status == SPRITE_STATUS_DESTROYED) then return end

    -- déplacement avec le clavier
    if (appState.currentScreen.name == SCREEN_PLAY) then
      if (lPlayer.moveWithKeys and lPlayer.checkIfActive()) then
        local angleOffset = 200 * pDt
        local speedFactor = 200 * pDt
          -- déplacements avec utilisation des vélocités
        if (love.keyboard.isDown(lPlayer.keys["moveRight"]) or love.keyboard.isDown(lPlayer.keys["moveRightAlt"])) then
          lPlayer.rotation = lPlayer.rotation + angleOffset
        end
        if (love.keyboard.isDown(lPlayer.keys["moveLeft"]) or love.keyboard.isDown(lPlayer.keys["moveLeftAlt"])) then
          lPlayer.rotation = lPlayer.rotation - angleOffset
        end
        if (love.keyboard.isDown(lPlayer.keys["moveUp"]) or love.keyboard.isDown(lPlayer.keys["moveUpAlt"])) then
          lPlayer.speed = lPlayer.speed + speedFactor
        end
        if (love.keyboard.isDown(lPlayer.keys["moveDown"]) or love.keyboard.isDown(lPlayer.keys["moveDownAlt"])) then
          lPlayer.speed = lPlayer.speed - speedFactor
        end

        -- on limite la valeur de l'angle à celle d'un cercle complet
        if (lPlayer.rotation > 360) then
          lPlayer.rotation = 0
        elseif (lPlayer.rotation < 0) then
          lPlayer.rotation = 360
        end

        --[[ TODO: determine la direction du joueur en fonction de l'angle de rotation
          lPlayer.direction = SPRITE_MOVE_RIGHT
          lPlayer.direction = SPRITE_MOVE_LEFT
          lPlayer.direction = SPRITE_MOVE_UP
          lPlayer.direction = SPRITE_MOVE_DOWN
        ]]

        -- limite la vélocité
        if (lPlayer.speed < 0) then
          lPlayer.speed = 0
        end
        if (lPlayer.speed > lPlayer.velocityMax) then
          lPlayer.speed = lPlayer.velocityMax
        end


      end --if (lPlayer.moveWithKeys and lPlayer..checkIfActive()) then

      -- seul le joueur utilise la friction et l'angle de rotation pour le calcul des vitesses
      if (lPlayer.useFriction) then

        -- Calcul des vélocités
        -- ******************************
        local rotationRadian = math.rad(lPlayer.rotation)
        local coeff = lPlayer.speed
        local implusionX = math.cos(rotationRadian) * coeff
        local implusionY = math.sin(rotationRadian) * coeff

        lPlayer.vX = lPlayer.vX + implusionX
        lPlayer.vY = lPlayer.vY + implusionY

        -- prise en compte de la friction de l'environnement et du poids
        local frictionAndWeight = lPlayer.weight * lPlayer.friction * pDt
        if (lPlayer.vX > 0) then
          lPlayer.vX = lPlayer.vX - frictionAndWeight
        elseif (lPlayer.vX < 0) then
          lPlayer.vX = lPlayer.vX + frictionAndWeight
        end
        if (lPlayer.vY > 0) then
          lPlayer.vY = lPlayer.vY - frictionAndWeight
        elseif (lPlayer.vY < 0) then
          lPlayer.vY = lPlayer.vY + frictionAndWeight
        end
        lPlayer.speed = lPlayer.speed - frictionAndWeight

        if (math.abs(lPlayer.speed) < 0.01) then lPlayer.speed = 0 end
        if (math.abs(lPlayer.vX) < 0.01) then lPlayer.vX = 0 end
        if (math.abs(lPlayer.vY) < 0.01) then lPlayer.vY = 0 end
      end -- if (useFriction) then
    end -- if (appState.currentScreen.name == SCREEN_PLAY) then
  end -- player.update

  --- Le joueur effectue une action (ie. tire un projectile)
  -- @param pType (OPTIONNEL) type de l'action (voir les constantes LAST_EXT_ACTIONXXX). Si absent, la valeur LAST_EXT_ACTION1 sera utilisée.
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lPlayer.characterAction = lPlayer.action
  function lPlayer.action (pType)
    -- debugFunctionEnter("player.action ",pType)
    lPlayer.characterAction(pType)

    if (not lPlayer.checkIfCanMakeAction()) then return end
    local lProjectile

    if (pType == LAST_EXT_ACTION2) then
      -- action  => Téléporte le joueur
      lPlayer.teleport()
    else
      -- action par défaut = action 1 => tir principal

      -- rappel:    createProjectile(pType                    , pImage                                               ,pX,pY,pLife,pEnergy,pDamages,pSpeed)
      lProjectile = createProjectile(SPRITE_TYPE_PROJ_PLAYER, FOLDER_PROJECTILES_IMAGES.."/projectile_player_1.ptxt", 0, 0, 1   , 1     , 1      , 300)
      lProjectile.color = lPlayer.color
      lProjectile.navPath = createNavPath(NAVPATH_TYPE_TOWARD_ROTATION, lProjectile, lPlayer)

      -- les sons et les animations doivent être joués dans le cet objet et pas dans son parent sinon les noms de fichiers chargés automatiquement ne seront pas les bons (liés au type de sprite)
      lPlayer.playSound("Action")
      lPlayer.playAnimation(ANIM_NAME_ACT_ACTION1)

      -- ajouter un effet de recul
      local rotationRadian = math.rad(lPlayer.rotation + 180)
      local coeff = 40
      local implusionX = math.cos(rotationRadian) * coeff
      local implusionY = math.sin(rotationRadian) * coeff

      lPlayer.vX = lPlayer.vX + implusionX
      lPlayer.vY = lPlayer.vY + implusionY
    end
  end -- player.action

  --- Téléporte le joueur vers une nouvelle position
  function lPlayer.teleport()
    -- debugFunctionEnter("player.teleport")
    local xMin, yMin, xMax, yMax
    xMin, yMin, xMax, yMax = lPlayer.getScreenLimits()

    local posX, posY
    posX, posY = findSecurePosition (xMin, yMin, xMax, yMax, 100, entities.NPCs)
    lPlayer.spawn(posX, posY)

  end
  -- initialisation par défaut
  lPlayer.initialize()

  return lPlayer
end -- createPlayer
