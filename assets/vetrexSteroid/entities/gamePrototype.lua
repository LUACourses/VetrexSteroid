-- Entité représentant un prototype de jeu
-- ****************************************

-- NOTE: SOUS-TYPE DE JEU doit être défini dans contants.lua car il est utilisé pour la définition des dossiers et avant l'auto load des modules.

--- Création d'un prototype de jeu
-- @return un pseudo objet gamePrototype
function createGameProtoype ()
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  -- debugFunctionEnter("createGameProtoype")"

	local lGamePrototype = {}

  --- Initialise l'objet
  function lGamePrototype.initialize ()
    debugFunctionEnter("gamePrototype.initialize")

    -- Redéfinit certains paramètres de configuration en fonction du prototype choisi
    love.window.setTitle("VetrexSteroid")
    love.window.setMode(1200, 900)
  end --gamePrototype.initialize

  --- Initialise le prototype
  function lGamePrototype.startGame ()
    debugFunctionEnter("gamePrototype.startGame")
		-- JEU DE TYPE TOPDOWN_SHOOTER
		-- ******************************
    debugMessage ("VetrexSteroid")

  	-- init de la camera, qui en se déplaçant automatiquement va permettre un scrolling
  	-- pas de scrolling auto pour ce jeu
    --camera.initialize(0, 60)
	end -- gamePrototype.startGame

  lGamePrototype.initialize()
	return lGamePrototype
end -- createGameProtoype
