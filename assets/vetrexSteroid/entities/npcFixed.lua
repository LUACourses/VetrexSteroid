-- Entité représentant une tourelle (pnj fixe)
-- Utilise une forme d'héritage simple: npcFixed qui inclus npc qui inclus sprite
-- ****************************************

-- SOUS-TYPE DE SPRITE
-- NOTE: la décomposition des constantes et le _ sont importants dans les libellés et les valeurs car cela permet de différencier les types et les sous-type et de faire des tests avec string:find
local SPRITE_TYPE_DETAIL = "npcFixed" -- cette variable doit être redéfinie dans chaque classe enfant
SPRITE_TYPE_NPC_FIXED = SPRITE_TYPE_NPC..NEXT_EXT_MOVEFIXED  -- pnj immobile
SPRITE_TYPE_NPC_FIXED1 = SPRITE_TYPE_NPC_FIXED..LAST_EXT_1 -- pnj immobile n°1, par défaut immobile et détruit en bas de l'écran (une tourelle)

--- Création d'un objet npcFixed
-- @param pX (OPTIONNEL) position en X (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pY (OPTIONNEL) position en Y (en pixel). Si absent la valeur par défaut des sprites sera utilisée.
-- @param pWeight (OPTIONNEL) poids du npc. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pLife (OPTIONNEL) nombre de vies initiale. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pEnergy (OPTIONNEL) niveau d'énergie initial. Si absent la valeur par défaut des sprites sera utilisée.
-- @param pImage (OPTIONNEL) image principale. Si absent la valeur FOLDER_NPCS_IMAGES.."/npcFixed.png" sera utilisée.
-- @param pSubType (OPTIONNEL) sous-type de pnj (voir les constantes SPRITE_TYPE_NPC_FIXED_XXX). Si absent, la valeur SPRITE_TYPE_NPC_FIXED1 sera utilisée.
-- @return un pseudo objet npcFixed
function createNpcFixed (pX, pY, pWeight, pLife, pEnergy, pImage, pSubType)
  -- NOTE: le nom de fonction createXXXX est réservé aux tables qui n'hérite pas de l'objet class
  debugFunctionEnter("createNpcFixed ", pX, pY, pWeight, pLife, pEnergy, pImage)
  if (pSubType == nil) then pSubType = SPRITE_TYPE_NPC_FIXED1 end
  local detailIndex = getLastSuffix(pSubType)

  if (pImage == nil) then pImage = FOLDER_NPCS_IMAGES.."/"..SPRITE_TYPE_DETAIL..detailIndex..".ptxt" end

  local lNpcFixed = createNPC(pSubType, pImage, pX, pY, pWeight, pLife, pEnergy)

  --- Initialise l'objet
  function lNpcFixed.initialize ()
    debugFunctionEnter("npcFixed.initialize")

    local detailIndex = getLastSuffix(lNpcFixed.type)
    local actionDelaiWithDtFactor = getDelaiDifficultyFactor()

    lNpcFixed.color = {10, 0, 160, 255}
    lNpcFixed.mustDisplayEnergy = false

    lNpcFixed.mustMoveWithCamera = true -- important

    -- ce type d'ennemi (un astéroïde fixe) ne tire pas
    lNpcFixed.canMakeAction = false

    --[[
    lNpcFixed.speed = 0
    lNpcFixed.actionDelai = {
      min = 4 * actionDelaiWithDtFactor,
      max = 6 * actionDelaiWithDtFactor
    }

    lNpcFixed.actionTimer = lNpcFixed.actionDelai.max or 1 * actionDelaiWithDtFactor
    ]]
    lNpcFixed.navPath = createNavPath(NAVPATH_TYPE_DOWN, lNpcFixed)

    -- ajoute les animations
    -- rappel: addAnimation (pName, pFolder, pExt, pSpeed, pIsLooping, pNextAnimationName, pSound, pMustPlaySound, pSoundIsLooping, pSoundEnd, pMustPlaySoundEnd, pEntityStatusWhenRunning, pEntityStatusWhenFinished, pCallBackWhenFinished)
    --lNpcFixed.addAnimation(ANIM_NAME_MOVE_WALK, FOLDER_NPCS_ANIMS.."/npcFixed1/walk")
    --lNpcFixed.playAnimation(ANIM_NAME_MOVE_WALK)

    -- définit les hotSpots pour les collisions
    lNpcFixed.createCollisionBox(COLLISION_MODEL_BOX4)

    lNpcFixed.updateInitialValues() -- important
  end -- npcFixed.initialize

  -- Override des fonctions du sprite
  -- ******************************

  --- affiche l'objet
  --[[ à décommenter pour des comportements spécifiques
  lNpcFixed.npcDraw = lNpcFixed.draw -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcFixed.draw ()
    ---- debugFunctionEnterNL("npcFixed.draw") -- ATTENTION  cet appel peut remplir le log
    -- affiche l'objet comme un sprite normal
     lNpcFall.npcDraw()
  end -- npcFixed.draw

  --- Détruit l'objet
  -- @param pMustAddScore true pour ajouter les points du sprite au score du joueur. Si absent, la valeur true sera utilisée.
  -- @param pMustShowFX true pour afficher un effet visuel lors de sa destruction. Si absent, sprite.mustDestroyedWithFX sera utilisé
  -- @param pMustPlaySound true pour jouer un son lors de sa destruction. Si absent, sprite.mustDestroyedWithSound sera utilisé
  lNpcFixed.npcDestroy = lNpcFixed.destroy -- mémorise la fonction du parent (object) afin de l'appeler si nécessaire
  function lNpcFixed.destroy (pMustAddScore, pMustShowFX, pMustPlaySound)
    debugFunctionEnter("npcFixed.destroy ", pMustAddScore, pMustShowFX, pMustPlaySound)

    -- la destruction effective se fait dans la classe parent
    lNpcFixed.npcDestroy()
  end -- npcFixed.destroy

  --- Actualise l'objet
  -- @param pDt delta time
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcFixed.npcUpdate = lNpcFixed.update
  function lNpcFixed.update (pDt)
    ---- debugFunctionEnterNL("npcFixed.update ", pDT) -- ATTENTION cet appel peut remplir le log
    lNpcFixed.npcUpdate(pDt)

  end -- npcFixed.update

  --- Vérifie les collisions de l'objet
  -- @return SPRITE_COLLISION_SPRITE ou SPRITE_COLLISION_NONE
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcFixed.npcCollide = lNpcFixed.collide
  function lNpcFixed.collide ()
    ---- debugFunctionEnterNL("npcFixed.collide") -- ATTENTION cet appel peut remplir le log
    local collisionType = lNpcFixed.npcCollide(pDt)

    if (not lNpcFixed.checkIfCanCollide()) then return SPRITE_COLLISION_NONE end

    return collisionType
  end -- npcFixed.collide
  ]]

  --- le PNJ effectue une action (tire un projectile)
  -- mémorise la fonction du parent pour pouvoir l'appeler dans l'objet enfant
  lNpcFixed.npcAction = lNpcFixed.action
  function lNpcFixed.action ()
    -- debugFunctionEnter("npc.action")
    lNpcFixed.npcAction()

    if (not lNpcFixed.checkIfCanMakeAction()) then return end
    local lProjectile

    -- COMPORTEMENT SPÉCIFIQUE: le projectile est dirigé vers le player
    -- ******************************
    lProjectile = createProjectile(SPRITE_TYPE_PROJ_NPC, FOLDER_PROJECTILES_IMAGES.."/projectile_npc_3.ptxt", 0, 0, 1   , 1     , 2      , 200)
    lProjectile.navPath = createNavPath(NAVPATH_TYPE_FROMTO, lProjectile, lNpcFixed, player)
    lProjectile.color = {255, 0, 255, 255 }
  end

  -- initialisation par défaut
  lNpcFixed.initialize()

  return lNpcFixed
end -- createDoor
